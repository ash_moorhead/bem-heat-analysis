#ifndef BEM_ANALYTICFUNCTIONS

using namespace std;

// Valid analytic value types
#define U_ANALYTIC_VALUE_TYPE 1
#define Q_ANALYTIC_VALUE_TYPE 2	

// Valid analytic function types
#define LAPLACE_2D_ANALTYIC_FUNCTION_1 1					// u(x,y) = x^2 + 2xy -y^2

// Valid analytic function errors
#define ANALYTIC_FUNCTIONS_NO_ERROR 0						// No error
#define ANALYTIC_FUNCTIONS_INVALID_ANALYSIS_TYPE 1			// The analysis type is not valid. See BEM_Constants.h for valid analysis types.
#define ANALYTIC_FUNCTIONS_INVALID_FUNCTION_ERROR 2			// The analysis function type is not valid. See above for valid analytic function types.
#define ANALYTIC_FUNCTIONS_INVALID_VALUE_TYPE_ERROR 3		// The analysis value type is not valid. See above for valid analytic value types.
#define ANALYTIC_FUNCTIONS_NOT_IMPLEMENTED_ERROR 4			// The code has not been implemented.

#include "BEM_Constants.h"

// Return the analytic function value for the given analysis, function and value type.
int BEMAnalyticFunction(
	const int analysisType,									// The analysis type for the analytic function. See BEM_Constants.h for valid analysis types.
	const int analyticFunction,								// The analytic function. See BEM_AnalyticFunctions.h for valid analytic function types.
	const int analyticValueType,							// The type of analytic value (u or q) to return. See BEM_AnalyticFunctions.h for valid analytic value types.
	double position[ MAX_NUM_DIMENSIONS ],					// position[ coordinateIdx ]. The coordinateIdx'th coordinate value to evaluate the analytic function at.
	double* analyticValue									// On exit, the value of the analytic function.
	);

#define BEM_ANALYTICFUNCTIONS
#endif