/*
	This file contains routines for evaluating the basis functions.

*/

#include "BEM_BasisFunctions.h"

// Evaluate a linear Lagrange basis function.
int BasisFunctionEvaluateLinearLagrangeXi(
	const int localNodeIdx,					// The local node index which identifies the particular basis function.									
	const int derivativeIdx,				// The partial derivative index of the basis funtion to evaluate. See BEM_BasisFunctions.h for valid partial derivative indices.								
	const double xi,						// The xi location to evaluate the basis function at.							
	double* phi								// On exit, the value of the evaluated basis function.				
	);

// Evaluate a quadratic Lagrange basis function.
int BasisFunctionEvaluateQuadraticLagrangeXi(
	const int localNodeIdx,					// The local node index which identifies the particular basis function.	
	const int derivativeIdx,				// The partial derivative index of the basis funtion to evaluate. See BEM_BasisFunctions.h for valid partial derivative indices.
	const double xi,						// The xi location to evaluate the basis function at.		
	double* phi								// On exit, the value of the evaluated basis function.
	);

// Evaluate a particular basis function at at particular xi location
int BasisFunctionEvaluateXi(
		const int elementType,				// The element type to evaluate the basis function for. See above for valid element types.
		const int localNodeIdx,				// The local node index which identifies the particular basis function.
		const int derivativeIdx,			// The partial derivative index of the basis funtion to evaluate. See BEM_BasisFunctions.h for valid partial derivative indices.
		const double xi,					// The xi location to evaluate the basis function at.
		double* phi							// On exit, the value of the evaluated basis function.
		)
{
	int err;

	switch (elementType) {
	case BASIS_FUNCTIONS_LINEAR_LAGRANGE_ELEMENT_TYPE:
		// Evaluate a linear Lagrange basis function.
		err = BasisFunctionEvaluateLinearLagrangeXi(localNodeIdx, derivativeIdx, xi, phi);
		if(err != BASIS_FUNCTIONS_NO_ERROR) return err;
		break;
	case BASIS_FUNCTIONS_QUADRATIC_LAGRANGE_ELEMENT_TYPE:
		// Evaluate a quadratic Lagrange basis function.
		err = BasisFunctionEvaluateQuadraticLagrangeXi(localNodeIdx, derivativeIdx, xi, phi);
		if(err != BASIS_FUNCTIONS_NO_ERROR) return err;
		break;
	default:
		return BASIS_FUNCTIONS_INVALID_ELEMENT_TYPE;
		break;
	}

	return	BASIS_FUNCTIONS_NO_ERROR;
}

// Evaluate a linear Lagrange basis function.
int BasisFunctionEvaluateLinearLagrangeXi(
	const int localNodeIdx,					// The local node index which identifies the particular basis function.									
	const int derivativeIdx,				// The partial derivative index of the basis funtion to evaluate. See BEM_BasisFunctions.h for valid partial derivative indices.								
	const double xi,						// The xi location to evaluate the basis function at.							
	double* phi								// On exit, the value of the evaluated basis function.				
	)
{
	switch (derivativeIdx) {
	case BASIS_FUNCTIONS_NO_PARTIAL_DERIVATIVE:
		switch (localNodeIdx) {	
		case 0:
			// phi_0 = 1 - xi
			*phi = 1.0 - xi;
			break;
		case 1:
			// phi_1 = xi
			*phi = xi;
			break;
		default:
			return BASIS_FUNCTIONS_INVALID_LOCAL_NODE_IDX_ERROR;
			break;
		}
		break;
	case BASIS_FUNCTIONS_FIRST_PARTIAL_DERIVATIVE:
		switch (localNodeIdx) {
		case 0:
			// d phi_0 \ d xi = -1
			*phi = -1.0;
			break;
		case 1:
			// d phi_1 \ d xi = 1
			*phi = 1.0;
			break;
		default:
			return BASIS_FUNCTIONS_INVALID_LOCAL_NODE_IDX_ERROR;
			break;
		}
		break;
	case BASIS_FUNCTIONS_SECOND_PARTIAL_DERIVATIVE:
		switch (localNodeIdx) {
		case 0:
			// d^2 phi_0 \ d xi^2 = 0
			*phi = 0.0;
			break;
		case 1:
			// d^2 phi_1 \ d xi^2 = 0
			*phi = 0.0;
			break;
		default:
			return BASIS_FUNCTIONS_INVALID_LOCAL_NODE_IDX_ERROR;
			break;
		}
		break;
	default:
		return BASIS_FUNCTIONS_INVALID_DERIVATIVE_IDX_ERROR;
		break;
	}

	return	BASIS_FUNCTIONS_NO_ERROR;
}

// Evaluate a quadratic Lagrange basis function.
int BasisFunctionEvaluateQuadraticLagrangeXi(
	const int localNodeIdx,					// The local node index which identifies the particular basis function.	
	const int derivativeIdx,				// The partial derivative index of the basis funtion to evaluate. See BEM_BasisFunctions.h for valid partial derivative indices.
	const double xi,						// The xi location to evaluate the basis function at.		
	double* phi								// On exit, the value of the evaluated basis function.
	)
{
	switch (derivativeIdx) {
	case BASIS_FUNCTIONS_NO_PARTIAL_DERIVATIVE:
		switch (localNodeIdx) {
		case 0:
			// phi_0 = 1 - 3xi + 2xi^2
			*phi = 1.0 - 3.0*xi + 2.0*xi*xi;
			break;
		case 1:
			// phi_1 = 4xi(1 - xi)
			*phi = 4.0*xi*(1.0 - xi);
			break;
		case 2:
			// phi_2 = xi(2xi - 1)
			*phi = xi*(2.0*xi - 1.0);
			break;
		default:
			return BASIS_FUNCTIONS_INVALID_LOCAL_NODE_IDX_ERROR;
			break;
		}
		break;
	case BASIS_FUNCTIONS_FIRST_PARTIAL_DERIVATIVE:
		switch (localNodeIdx) {
		case 0:
			// d phi_0 \ d xi = 4xi - 3
			*phi = 4.0*xi - 3.0;
			break;
		case 1:
			// d phi_1 \ d xi = 4 - 8xi
			*phi = 4.0 - 8.0*xi;
			break;
		case 2:
			// d phi_1 \ d xi = 4xi - 1
			*phi = 4.0*xi - 1.0;
			break;
		default:
			return BASIS_FUNCTIONS_INVALID_LOCAL_NODE_IDX_ERROR;
			break;
		}
		break;
	case BASIS_FUNCTIONS_SECOND_PARTIAL_DERIVATIVE:
		switch (localNodeIdx) {
		case 0:
			// d^2 phi_0 \ d xi^2 = 4
			*phi = 4.0;
			break;
		case 1:
			// d^2 phi_0 \ d xi^2 = -8
			*phi = -8.0;
			break;
		case 2:
			// d^2 phi_0 \ d xi^2 = 4
			*phi = 4.0;
			break;
		default:
			return BASIS_FUNCTIONS_INVALID_LOCAL_NODE_IDX_ERROR;
			break;
		}
		break;
	default:
		return BASIS_FUNCTIONS_INVALID_DERIVATIVE_IDX_ERROR;
		break;
	}

	return	BASIS_FUNCTIONS_NO_ERROR;
}

// Interpolate an elemental basis function at at particular xi location
int BasisFunctionInterpolateXi(
	const int elementType, 					// The element type to interpolate the basis function for. See above for valid element types.
	const int derivativeIdx,				// The partial derivative index of the funtion to interpolate. See above for valid partial derivative indices.
	const double xi,						// The xi location to interpolate the function at.
	const double* dofValues,				// dofValues[ localDofIdx ]. The element degrees-of-freedom for the interpolation.
	double* phi								// On exit, the value of the interpolated function.
	)
{
	int err, localNodeIdx;
	double bfValue;

	*phi = 0.0;
	for(localNodeIdx = 0; localNodeIdx < elementType + 1; localNodeIdx++) {
		err = BasisFunctionEvaluateXi(elementType, localNodeIdx, derivativeIdx, xi, &bfValue);
		if(err != BASIS_FUNCTIONS_NO_ERROR) return(err);
		*phi += bfValue*dofValues[ localNodeIdx ];
	}

	return	BASIS_FUNCTIONS_NO_ERROR;
}















