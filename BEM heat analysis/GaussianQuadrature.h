#ifndef GAUSSIAN_QUADRATURE

#include <stdlib.h>
#include <math.h>

using namespace std;

// Valid Gaussian quadrature errors
#define GAUSSIAN_QUADRATURE_NO_ERROR 0							// No error.
#define GAUSSIAN_QUADRATURE_INVALID_NUM_GAUSS_POINTS_ERROR 1	// The requested number of Gauss points is invalid.
#define GAUSSIAN_QUADRATURE_XI_NOT_NULL_ERROR 2					// The supplied xi pointer is not null.
#define GAUSSIAN_QUADRATURE_WGT_NOT_NULL_ERROR 3				// The supplied wgt pointer is not null.

#define GAUSSIAN_LEGENDRE_QUADRATURE_MAX_NUM_GAUSS_POINTS 64	//The maximum number of Gaussian Legendre points.

// This routine creates the weights and abscissae for a logarithmic Gaussian quadrature scheme i.e., \int_0^1 f(x)ln(x)dx
int GaussianQuadratureLogarithmicPointsCreate(
	const int N,			// The required number of requested logarithmic points.
	double **xi,			// On exit, xi[i] the i'th logarithmic Gaussian quadrature location.
	double **wgt			// On exit, wgt[i] the i'th logarithmic Gaussian quadrature weight.
	);	

// This routine creates the weights and abscissae for a Gaussian Legendre quadrature scheme i.e., \int_alpha^beta f(x)dx
int GaussianQuadratureLegendrePointsCreate(
	const int N, 			// The required number of requested Legendre points.
	const double alpha,		// The integral start
	const double beta,		// The integral finish
	double **xi,			// On exit, xi[i] the i'th Legendre Gaussian quadrature location.
	double **wgt			// On exit, wgt[i] the i'th Legendre Gaussian quadrature weight.
	);	

// This routine destroys the Gaussian quadrature location and weight arrays.
int GaussianQuadraturePointsDestroy(
	double **xi,			// The Gaussian quadrature xi locations to destroy.
	double **wgt			// The Gaussian quadrature weights to destroy.	
	);	

#define GAUSSIANQUADRATURE
#endif
