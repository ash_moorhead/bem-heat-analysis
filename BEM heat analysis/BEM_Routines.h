#ifndef BEM_ROUTINES

#include "BEM_Constants.h"
#include <stdlib.h>
#include <math.h>

using namespace std;

// Valid BEM routines error codes.
#define BEM_INVALID_NODE_IDX_ERROR 1					// The supplied node index is not valid.
#define BEM_INVALID_ELEMENT_IDX_ERROR 2					// The supplied element index is not valid.
#define BEM_INVALID_GREENS_FUNCTION_IDX_ERROR 3			// The supplied Green's function index is not valid. See below for valid Green's function indices.
#define BEM_ZERO_DISTANCE_ERROR 4						// There is a zero distance division error.
#define BEM_ZERO_JACOBIAN_ERROR 5						// The calculated Jacobian is zero.
#define BEM_INVALID_ANALYSIS_TYPE_ERROR 6				// The supplied analysis type is not valid. See BEM_Constants.h for valid analysis types.

// Valid Green's function types.
#define BEM_LAPLACE_GREENS_FUNCTION 1					// A standard Laplace Green's function.
#define BEM_LOG_LAPLACE_GREENS_FUNCTION 2				// A modified Laplace Green's function for logarithmic quadrature.
#define BEM_DISPLACEMENT_GREENS_FUNCTION 3				// A displacement Kelvin solution for linear elasticity.
#define BEM_TRACTION_GREENS_FUNCTION 4					// A traction Kelvin solution for linear elasticity.

// Valid Gaussian quadrature constants.
#define BEM_NUMBER_GAUSS_SCHEMES 4						// The number of Gaussian quadrature integration schemes.
#define BEM_LOG_GAUSS_INTEGRATION_SCHEME 0				// A logarithmic Gaussian quadrature integration scheme.
#define BEM_LOW_GAUSS_INTEGRATION_SCHEME 1				// A low number of Gauss points Gaussian Legendre integration scheme.
#define BEM_MEDIUM_GAUSS_INTEGRATION_SCHEME 2			// A medium number of Gauss points Gaussian Legendre integration scheme.
#define BEM_HIGH_GAUSS_INTEGRATION_SCHEME 3				// A high number of Gauss points Gaussian Legendre integration scheme.
#define BEM_INSIDE_ELEMENT_INTEGRATION_SCHEME 4			// A singularity is inside the current element integration scheme.

// Define the number of Gauss points for the integration schemes.
#define BEM_NUMBER_LOG_GAUSS_POINTS 5					// The number of Gauss points for a logarithmic Gaussian quadrature integration scheme.
#define BEM_NUMBER_LOW_GAUSS_POINTS 3					// The number of Gauss points in the low number of Gauss points Gaussian Legendre integration scheme.
#define BEM_NUMBER_MEDIUM_GAUSS_POINTS 5				// The number of Gauss points in the medium number of Gauss points Gaussian Legendre integration scheme.
#define BEM_NUMBER_HIGH_GAUSS_POINTS 10					// The number of Gauss points in the high number of Gauss points Gaussian Legendre integration scheme.

// Valid row sum flags.
#define BEM_ROW_SUMS 1									// Use row sums to calculate the diagonal entry in the A matrix.
#define BEM_NO_ROW_SUMS 2								// Do not use row sums to calculate the diagonal entry in the A matrix.

// Calculates the geometric information (location, tangent, normal, Jacobian etc.) at a xi location in an element.
int CalculateGeometricInformation(
	const int numDimensions,				// The number of dimensions
	const int elementType,					// The type of element
	const double xi,						// The xi location to calculate the geometric information at
	double** elementDofs,					// The element degrees-of-freedom for this element
	double location[ MAX_NUM_DIMENSIONS ],	// location[ coordinateIdx ]. On exit, the coordinateIdx'th coordinate of the location of the xi point.
	double tangent[ MAX_NUM_DIMENSIONS ],	// tangent[ coordinateIdx ]. On exit, the coordinateIdx'th coordinate of the tangent at the xi point.
	double normal[ MAX_NUM_DIMENSIONS ],	// normal[ coordinateIdx ]. On exit, the coordinateIdx'th coordiante of the normal at the xi point.
	double* Jacobian						// On exit, the Jacobian of the coordinate to xi transformation at the xi point.
	);

// This routine calculates the A & B matrix entries for a given singular point
int CalculateMatrixEntries(
	const int numDimensions,			// The number of dimensions in the problem.
	const int elementType,				// The boundary element type in the mesh.
	const int numElements,				// The number of elements in the mesh.
	const double averageElementSize,	// The average size of elements in the mesh.
	const int analysisType,				// The type of analysis (equation type).
	const int numVariables,				// The number of variables per node.
	const int numDOFS,					// The number of degrees-of-freedom (DOFS) in the problem.
	int** elementNodes,					// elementNodes[ elementIdx ][ localNodeIdx ]. The node number for the localNodeIdx'th local node in the elementIdx'th element.
	double pLocation[ MAX_NUM_DIMENSIONS ],	// pLocation[ coordinateIdx ]. The coordinateIdx'th coordinate for the location of the singular point.
	double **geometry,					// geometry[ nodeIdx ][ coordinateIdx ]. The coordinateIdx'th geometric coordinate for the nodeIdx'th node.
	double** elementDOFS,				// elementDOFS[ coordinateIdx ][ localNodeIdx ]. Temporary working array to store the element DOFS for the coordinateIdx'th coordinate of the localNodeIdx'th local node in an element.
	int* numGaussPoints,				// numGaussPoints[ integrationSchemeIdx ]. The number of Gauss points in the integrationSchemeIdx'th integration scheme.
	double** gaussLocations,			// gaussLocations[ integrationSchemeIdx ][ gaussPointIdx ]. The xi location of the gaussPointIdx'th Gauss point in the integrationSchemeIdx'th integration scheme.
	double** gaussWeights,				// gaussWeights[ integrationSchemeIdx ][ gaussPointIdx ]. The Gauss weight of the gaussPointIdx'th Gauss point in the integrationSchemeIdx'th integration scheme.
	const int rowSumsFlag,				// Flag to indicate if row sums are used to calculate the diagonal entry in the A matrix.
	const int rowDofIdx,				// The row (DOF) index in the A & B matrices to calculate the matrix entries for.
	double** A,							// The BEM A matrix to calculate the matrix entries for.
	double** B							// The BEM B matrix to calculate the matrix entries for.
	);

// Setup integration schemes
int IntegrationSchemesSetup(
	int **numGaussPoints,			// numGaussPoints[ integrationSchemeIdx ]. On exit, the number of Gauss points for the integrationIdx'th integration scheme.
	double ***gaussLocations,		// gaussLocations[ integrationSchemeIdx ][ gaussPointIdx ]. On exit, the xi location for the gaussPointIdx'th gauss point in the integrationSchemeIdx'th integration scheme.
	double ***gaussWeights			// gaussWeights[ integrationSchemeIdx ][ gaussPointIdx ]. On exit, the weight for the gaussPointIdx'th gauss point in the integrationSchemeIdx'th integration scheme.
	);

// Finalise the integration schemes
int IntegrationSchemesFinalise(
	int **numGaussPoints,			// The number of Gauss points array to finalise
	double ***gaussLocations,		// The Gauss point locations array to finalise
	double ***gaussWeights			// The Gauss weights array to finalise
	);

int BoundaryConditions(double **A, double **B, int **nodeBoundaryConditions, double *f, double *u, double *q, int numElements, double **B_new, double **C);
int Rearranging(int **nodeBoundaryConditions, double *u, double *q, double *x, int numElements);

#define BEM_ROUTINES
#endif
