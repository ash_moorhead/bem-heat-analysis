/*
	This file contains routines for BEM setup (generate geometry etc.)
*/

#include "BEM_SetupAndFinalise.h"
#include "BEM_Constants.h"
#include "BEM_BasisFunctions.h"
#include "BEM_AnalyticFunctions.h"
#include "Constants.h"
#include "CommandLineFilter.h"
#include "Misc.h"

// Setup the BEM problem
int BEMSetupProblem(
	CommandLineFilter *cmd,			//The pointer to the command line filter config file
	int *numDimensions				//On exit, the number of dimensions of the problem
	)
{
	// Local variables	
	ifstream ifid;

	// Check incoming pointers
	if(cmd == NULL) {
		cout << "ERROR: The command line filter is NULL."<<endl;
		BEM_ERROR
	}

	// Get the problem dimension
	if(cmd->IsPresent('d')) {
		*numDimensions = cmd->GetInt('d');
		// Check input is valid
		if(*numDimensions < 1 || *numDimensions > MAX_NUM_DIMENSIONS) {
			cout << "ERROR: The number of dimensions of "<<*numDimensions<<" is invalid. The number of dimensions must be >= 0 and <= 3."<<endl;
			BEM_ERROR
		}
		// For now just alow 2D
		if(*numDimensions != 2) {
			cout << "ERROR: A dimensions of "<<numDimensions<<" is invalid for BEM."<<endl;
			BEM_ERROR
		}
	} else {
		*numDimensions = 2;
	}

	return BEM_NO_ERROR;
}

// Finalise the BEM Problem and deallocate any variables.
int BEMFinaliseProblem(
	)
{
	//Nothing to do

	return BEM_NO_ERROR;
}

// Setup the mesh and geometric information
int BEMSetupGeometry(
	CommandLineFilter *cmd,			//The pointer to the command line filter config file
	int numDimensions,				//The number of dimensions of the problem
	int *meshType,					//On exit, the type of mesh
	int *elementType,				//On exit, the type of boundary element
	int *numNodes,					//On exit, the number of nodes in the problem
	int *numElements,				//On exit, the number of elements in the problem
	int *nodesPerElement,			//On exit, the number of nodes per element
	int ***elementNodes,			//On exit, the nodes in each element
	int **nodeCorners,				//On exit, the nodes corner array
	double ***geometry,				//On exit, the nodal geometry
	double *averageElementSize		//On exit, the average element size
	)
{
	// Local variables	
	int coordinateIdx,cornerIdx,cornerNode,cornerType,elementIdx,elementOffset,elementNode,localNodeIdx;
	int nodeIdx,nodeOffset,numCircumfrential,numCorners,numHeight,numLength;
	double circleRadius,plateHeight,plateLength,theta;
	ifstream ifid;

	// Check incoming pointers
	if(cmd == NULL) {
		cout << "ERROR: The command line filter is NULL."<<endl;
		BEM_ERROR
	}
	if(*elementNodes != NULL) {
		cout << "ERROR: elementNodes pointer is not NULL."<<endl;
		BEM_ERROR
	}
	if(*nodeCorners != NULL) {
		cout << "ERROR: nodeCorners pointer is not NULL."<<endl;
		BEM_ERROR
	}
	if(*geometry != NULL) {
		cout << "ERROR: geometry pointer is not NULL."<<endl;
		BEM_ERROR
	}

	// Check dimension
	if(numDimensions != 2) {
		cout << "ERROR: Only two dimensions are implemented."<<endl;
		BEM_ERROR
	}

	//Check if we are generating a mesh or reading it in
	if(cmd->IsPresent('g')) {
		// Get the element type
		if(cmd->IsPresent('s')) {
			// Check input is valid
			*elementType = cmd->GetInt('s');
			switch(cmd->GetInt('s')) {
			case 1:	
				*elementType = BASIS_FUNCTIONS_LINEAR_LAGRANGE_ELEMENT_TYPE;
				break;
			case 2:	
				*elementType = BASIS_FUNCTIONS_QUADRATIC_LAGRANGE_ELEMENT_TYPE;
				break;
			default:			
				cout << "ERROR: The element type of "<<cmd->GetInt('s')<<" is not valid for BEM."<<endl;
				BEM_ERROR
				break;
			} 
		} else {
			*elementType = BASIS_FUNCTIONS_LINEAR_LAGRANGE_ELEMENT_TYPE;
		}

		if(!strcmp(cmd->GetString('g'),"Circle")) {
			*meshType = CIRCLE_GENERATED_MESH;
		} else if(!strcmp(cmd->GetString('g'),"Plate")) {
			*meshType = PLATE_GENERATED_MESH;
		} else {
			cout << "ERROR: Generated mesh type command string of "<<cmd->GetString('g')<<" is unknown."<<endl;
			BEM_ERROR
		}
		switch (*meshType) {
		case CIRCLE_GENERATED_MESH:

			// Generate a circular mesh specified as follows:
			//			-R radius
			//			-r number of circumfrential elements
			//
			// -----------------------------------------------------------------------------------------------------------

			// Get the circle radius
			if(cmd->IsPresent('R')) {
				circleRadius = cmd->GetDouble('R');
				// Check the input
				if(circleRadius < ZERO_TOLERANCE) {
					cout << "ERROR: The circle raduis of "<<circleRadius<<" is invalid. The circle radius must be > 0"<<endl;
					BEM_ERROR
				}
			} else {
				circleRadius = 2.0;
			}
			// Get the number of circumfrential elements
			if(cmd->IsPresent('r')) {
				numCircumfrential = cmd->GetInt('r');
				// Check the input
				if(numCircumfrential < 3) {
					cout << "ERROR: The number of circumfrential elements of "<<numCircumfrential<<" is invalid. The number of elements must be > 2"<<endl;
					BEM_ERROR
				}
			} else {
				numCircumfrential = 8;
			}

			// Generate the circular mesh

			// Generate the nodes
			*nodesPerElement = *elementType + 1;
			*numNodes = numCircumfrential*(*nodesPerElement - 1);
			// Allocating space to store the node points:
			*geometry = new double* [ *numNodes ];
			for(nodeIdx = 0; nodeIdx < *numNodes; nodeIdx++) {
				(*geometry)[ nodeIdx ] = new double [ numDimensions ];
			}
			// Set the node positions
			for(nodeIdx = 0; nodeIdx < *numNodes; nodeIdx++) {
				theta = nodeIdx*2.0*PI/(*numNodes);
				(*geometry)[ nodeIdx ][ 0 ] = circleRadius*cos(theta);
				(*geometry)[ nodeIdx ][ 1 ] = circleRadius*sin(theta);
			}
			// Generate the elements
			*numElements = numCircumfrential;
			// Allocating space for the element connectivity array:
			*elementNodes = new int* [ *numElements ];
			for(elementIdx = 0; elementIdx < *numElements; elementIdx++) {
				(*elementNodes)[ elementIdx ] = new int [ *nodesPerElement ];
			}
			for(elementIdx = 0; elementIdx < *numElements; elementIdx++) {
				// Set up the element connectivity
				for(localNodeIdx = 0; localNodeIdx < *nodesPerElement; localNodeIdx++) {
					(*elementNodes)[ elementIdx ][ localNodeIdx ] = elementIdx*((*nodesPerElement) - 1) + localNodeIdx;
				}
			}
			(*elementNodes)[ *numElements - 1 ][ *nodesPerElement - 1 ] = 0;

			// Allocating space to store the corners:
			*nodeCorners = new int [ *numNodes ];
			for(nodeIdx = 0; nodeIdx < *numNodes; nodeIdx++) {
				(*nodeCorners)[ nodeIdx ] = NODE_NO_CORNER;
			}

			*averageElementSize = 2.0*PI*circleRadius/(double)numCircumfrential;

			break;
		case PLATE_GENERATED_MESH:

			// Generate a plate with a circular hole mesh specified as follows:
			//			-R radius of inner hole
			//			-r number of circumfrential elements in the inner hole
			//          -L length of the plate
			//          -l number of elements along the plate's length
			//          -H height of the plate
			//          -h number of elements along the plate's height
			//
			// -----------------------------------------------------------------------------------------------------------

			// Get the hole radius
			if(cmd->IsPresent('R')) {
				circleRadius = cmd->GetDouble('R');
				// Check the input
				if(circleRadius < ZERO_TOLERANCE) {
					cout << "ERROR: A circleRadius of "<<circleRadius<<" is invalid. The radius must be > 0.0."<<endl;
					BEM_ERROR
				}
			} else {
				circleRadius = 1.0;
			}
			// Get the number of circumfrential elements in the hole
			if(cmd->IsPresent('r')) {
				numCircumfrential = cmd->GetInt('r');
				// Check the input
				if(numCircumfrential < 3) {
					cout << "ERROR: The number of circumfrential elements of "<<numCircumfrential<<" is invalid. The number of elements must be > 2."<<endl;
					BEM_ERROR
				}
			} else {
				numCircumfrential = 8;
			}
			// Get the plate Length
			if(cmd->IsPresent('L')) {
				plateLength = cmd->GetDouble('L');
				// Check the input
				if(plateLength < ZERO_TOLERANCE) {
					cout << "ERROR: The plate length of "<<plateLength<<" is invalid. The plate length must be > 0.0."<<endl;
					BEM_ERROR
				}
			} else {
				plateLength = 15.0;
			}
			// Get the number of length elements
			if(cmd->IsPresent('l')) {
				numLength = cmd->GetInt('l');
				// Check the input
				if(numLength < 1) {
					cout << "ERROR: The number of length elements of "<<numLength<<" is invalid. The number of elements must be > 0"<<endl;
					BEM_ERROR
				}
			} else {
				numLength = 15;
			}
			// Get the plate height
			if(cmd->IsPresent('H')) {
				plateHeight = cmd->GetDouble('H');
				// Check the input
				if(plateHeight < ZERO_TOLERANCE) {
					cout << "ERROR: The plate height of "<<plateHeight<<" is invalid. The plate length must be > 0.0."<<endl;
					BEM_ERROR
				}
			} else {
				plateHeight = 10.0;
			}
			// Get the number of height elements
			if(cmd->IsPresent('h')) {
				numHeight = cmd->GetInt('h');
				// Check the input
				if(numHeight < 1) {
					cout << "ERROR: The number of height elements of "<<numHeight<<" is invalid. The number of elements must be > 0"<<endl;
					BEM_ERROR
				}
			} else {
				numHeight = 10;
			}
			// Generate the plate mesh

			// Generate the nodes and elements
			*nodesPerElement = *elementType + 1;
			*numElements = 2*numHeight + 2*numLength + numCircumfrential;
			*numNodes = (*numElements)*((*nodesPerElement) - 1);
			// Allocating space to store the node points and element connectivity array:
			*geometry = new double* [ *numNodes ];
			for(nodeIdx = 0; nodeIdx < *numNodes; nodeIdx++) {
				(*geometry)[ nodeIdx ] = new double [ numDimensions ];
			}
			*elementNodes = new int* [ *numElements ];
			for(elementIdx = 0; elementIdx < *numElements; elementIdx++) {
				(*elementNodes)[ elementIdx ] = new int [ *nodesPerElement ];
			}
			// Allocating space to store the corners:
			*nodeCorners = new int [ *numNodes ];
			for(nodeIdx = 0; nodeIdx < *numNodes; nodeIdx++) {
				(*nodeCorners)[ nodeIdx ] = NODE_NO_CORNER;
			}
			//Do right plate edge
			nodeOffset = 0;
			elementOffset = 0;
			for(nodeIdx = 0; nodeIdx < numHeight*((*nodesPerElement) - 1); nodeIdx++) {
				(*geometry)[ nodeIdx + nodeOffset ][ 0 ] = plateLength/2.0;
				(*geometry)[ nodeIdx + nodeOffset ][ 1 ] = nodeIdx*plateHeight/(numHeight*((*nodesPerElement) - 1)) - plateHeight/2.0;
			}
			for(elementIdx = 0; elementIdx < numHeight; elementIdx++) {
				for(localNodeIdx = 0; localNodeIdx < (*nodesPerElement); localNodeIdx++) {
					(*elementNodes)[ elementIdx + elementOffset ][ localNodeIdx ] = elementIdx*((*nodesPerElement) - 1) + localNodeIdx + nodeOffset;
				}
			}
			(*nodeCorners)[0] = NODE_90DEG_CORNER;
			nodeOffset += numHeight*((*nodesPerElement) - 1);
			elementOffset += numHeight;
			(*elementNodes)[ elementOffset - 1 ][ (*nodesPerElement) - 1] = nodeOffset;
			//Do top plate edge
			for(nodeIdx = 0; nodeIdx < numLength*((*nodesPerElement) - 1); nodeIdx++) {
				(*geometry)[ nodeIdx + nodeOffset ][ 0 ] = plateLength/2.0 - nodeIdx*plateLength/(numLength*((*nodesPerElement) - 1));
				(*geometry)[ nodeIdx + nodeOffset ][ 1 ] = plateHeight/2.0;
			}
			for(elementIdx = 0; elementIdx < numLength; elementIdx++) {
				for(localNodeIdx = 0; localNodeIdx < (*nodesPerElement); localNodeIdx++) {
					(*elementNodes)[ elementIdx + elementOffset ][ localNodeIdx ] = elementIdx*((*nodesPerElement) - 1) + localNodeIdx + nodeOffset;
				}
			}
			(*nodeCorners)[ nodeOffset ] = NODE_90DEG_CORNER;
			nodeOffset += numLength*((*nodesPerElement) - 1);
			elementOffset += numLength;
			(*elementNodes)[ elementOffset - 1 ][ (*nodesPerElement) - 1] = nodeOffset;
			//Do left plate edge
			for(nodeIdx = 0; nodeIdx < numHeight*((*nodesPerElement) - 1); nodeIdx++) {
				(*geometry)[ nodeIdx + nodeOffset ][ 0 ] = -plateLength/2.0;
				(*geometry)[ nodeIdx + nodeOffset ][ 1 ] = plateHeight/2.0 - nodeIdx*plateHeight/(numHeight*((*nodesPerElement) - 1));
			}
			for(elementIdx = 0; elementIdx < numLength; elementIdx++) {
				for(localNodeIdx = 0; localNodeIdx < (*nodesPerElement); localNodeIdx++) {
					(*elementNodes)[ elementIdx + elementOffset ][ localNodeIdx ] = elementIdx*((*nodesPerElement) - 1) + localNodeIdx + nodeOffset;
				}
			}
			(*nodeCorners)[nodeOffset] = NODE_90DEG_CORNER;
			nodeOffset += numHeight*((*nodesPerElement) - 1);
			elementOffset += numHeight;
			(*elementNodes)[ elementOffset - 1 ][ (*nodesPerElement) - 1 ] = nodeOffset;
			//Do bottom plate edge
			for(nodeIdx = 0; nodeIdx < numLength*((*nodesPerElement) - 1); nodeIdx++) {
				(*geometry)[ nodeIdx + nodeOffset ][ 0 ] = nodeIdx*plateLength/(numLength*((*nodesPerElement) - 1)) - plateLength/2.0;
				(*geometry)[ nodeIdx + nodeOffset ][ 1 ] = -plateHeight/2.0;
			}
			for(elementIdx = 0; elementIdx < numLength; elementIdx++) {
				for(localNodeIdx = 0; localNodeIdx < (*nodesPerElement); localNodeIdx++) {
					(*elementNodes)[ elementIdx + elementOffset ][ localNodeIdx ] = elementIdx*((*nodesPerElement) - 1) + localNodeIdx + nodeOffset;
				}
			}
			(*nodeCorners)[ nodeOffset ] = NODE_90DEG_CORNER;
			nodeOffset += numLength*((*nodesPerElement) - 1);
			elementOffset += numLength;
			(*elementNodes)[ elementOffset - 1 ][ (*nodesPerElement) - 1 ] = 0;
			//Do plate hole
			for(nodeIdx = 0; nodeIdx < numCircumfrential*((*nodesPerElement) - 1); nodeIdx++) {
				theta = 2.0*PI - nodeIdx*2.0*PI/(numCircumfrential*((*nodesPerElement) - 1));
				(*geometry)[ nodeIdx + nodeOffset ][ 0 ] = circleRadius*cos(theta);
				(*geometry)[ nodeIdx + nodeOffset ][ 1 ] = circleRadius*sin(theta);
			}
			for(elementIdx = 0; elementIdx < numCircumfrential; elementIdx++) {
				for(localNodeIdx = 0; localNodeIdx < (*nodesPerElement); localNodeIdx++) {
					(*elementNodes)[ elementIdx + elementOffset ][ localNodeIdx ] = elementIdx*((*nodesPerElement) - 1) + localNodeIdx + nodeOffset;
				}
			}
			(*elementNodes)[ (*numElements) - 1 ][ (*nodesPerElement) - 1 ] = nodeOffset;

			*averageElementSize = (2.0*PI*circleRadius/(double)numCircumfrential + plateLength/(double)numLength + plateHeight/(double)numHeight )/3.0;

			break;
		default:
			cout << "ERROR: generatedMeshType of "<<*meshType<<" is not defined."<<endl;
			BEM_ERROR
			break;
		}
	} else {

		// Reading in the problem data from the files specified as follows:
		//			-n nodefilename
		//			-e elementfilename
		//          -C cornerfilename (if required)
		//
		// -----------------------------------------------------------------------------------------------------------

		*meshType = READIN_MESH;

		// Reading in the node positions:
		ifid.open(cmd->GetString('n'));
		ifid >> *numNodes;	// numNodes is the number of nodes

		// Allocating space to store these points:
		*geometry = new double* [ *numNodes ];
		for(nodeIdx = 0; nodeIdx < *numNodes; nodeIdx++) {
			(*geometry)[ nodeIdx ] = new double [ numDimensions ];
		}

		// Reading in the node positions from the file specified by -n
		for(nodeIdx = 0; nodeIdx < *numNodes; nodeIdx++) {
			for(coordinateIdx = 0; coordinateIdx < numDimensions; coordinateIdx++) {
				ifid >> (*geometry)[ nodeIdx ][ coordinateIdx ];
			}
		}

		ifid.close();

		// We need to read in the connectivity between nodes within an element: 
		ifid.open(cmd->GetString('e'));		// Opening the file specified by the -e flag in the config file
		ifid >> *numElements >> *nodesPerElement;
		// Check input
		if(*numElements > 0) {
			switch( *nodesPerElement ) {
			case(2):
				*elementType = BASIS_FUNCTIONS_LINEAR_LAGRANGE_ELEMENT_TYPE;
				break;
			case(3):	
				*elementType = BASIS_FUNCTIONS_QUADRATIC_LAGRANGE_ELEMENT_TYPE;
				break;
			default:			
				cout << "ERROR: The number of nodes per element of "<<*nodesPerElement<<" is not valid."<<endl;
				BEM_ERROR
				break;
			} 
			// Allocating space for the element connectivity array:
			*elementNodes = new int* [ *numElements ];
			for(elementIdx = 0; elementIdx < *numElements; elementIdx++) {
				(*elementNodes)[ elementIdx ] = new int [ *nodesPerElement ];
			}
			// Read in element connectivity
			for(elementIdx = 0; elementIdx < *numElements; elementIdx++) {
				for(localNodeIdx = 0; localNodeIdx < *nodesPerElement; localNodeIdx++) {
					ifid >> elementNode;
					// Check input
					if(elementNode >= 0 && elementNode < *numNodes) {
						(*elementNodes)[ elementIdx ][ localNodeIdx ] = elementNode;
					} else {
						cout << "ERROR: The node number of "<<elementNode<<" for local node "<<localNodeIdx<<" of element "<<elementIdx<<" is not valid. It must be >= 0 and < "<<*numNodes<<endl;
						BEM_ERROR
					}
				}
			}
		} else {
			cout << "ERROR: The number of elements of "<<*numElements<<" is not valid. It must be > 0."<<endl;
			BEM_ERROR
		}
		ifid.close();

		// Allocating space to store these corners:
		*nodeCorners = new int [ *numNodes ];
		for(nodeIdx = 0; nodeIdx < *numNodes; nodeIdx++) {
			(*nodeCorners)[ nodeIdx ] = NODE_NO_CORNER;
		}
		// Read in any corners file
		if(cmd->IsPresent('C')) {
			ifid.open(cmd->GetString('C'));
			ifid >> numCorners;
			for(cornerIdx = 0; cornerIdx < numCorners; cornerIdx++) {
				ifid >> cornerNode >> cornerType;
				if(cornerNode >= 0 && cornerNode < *numNodes) {
					switch (cornerType) {
					case NODE_NO_CORNER:
						//Do nothing
						break;
					case NODE_90DEG_CORNER:
						(*nodeCorners)[ cornerNode ] = NODE_90DEG_CORNER;
						break;
					default:
						cout << "ERROR: A corner type of "<<cornerType<<" for corner node "<<cornerNode<<" is not valid."<<endl;
						BEM_ERROR
						break;
					}
				} else {
					cout << "ERROR: A corner node number of "<<cornerNode<<" is not valid."<<endl;
					BEM_ERROR
				}
			}
			ifid.close();
			*averageElementSize = 0.0;
		}
	}
	
	return BEM_NO_ERROR;
}

// Finalise the Geometry and deallocate any variables.
int BEMFinaliseGeometry(
	int numNodes,					//The number of nodes in the problem
	int numElements,				//The number of nodes in the problem
	int ***elementNodes,			//The nodes in each element array to finalise
	int **nodeCorners,				//The nodes corner array to finalise
	double ***geometry				//The nodal geometry to finalise
	)
{
	// Local variables
	int elementIdx, nodeIdx;

	if(*elementNodes != NULL) {
		for(elementIdx = 0; elementIdx < numElements; elementIdx++) {
			if((*elementNodes)[ elementIdx ] != NULL) delete [] (*elementNodes)[ elementIdx ];
		}
		delete [] *elementNodes;
	}
	if(*nodeCorners != NULL) delete [] *nodeCorners;
	if(*geometry != NULL) {
		for(nodeIdx = 0; nodeIdx < numNodes; nodeIdx++) {
			if((*geometry)[ nodeIdx ] != NULL) delete [] (*geometry)[ nodeIdx ];
		}
		delete [] *geometry;
	}
	return BEM_NO_ERROR;
}

// Setup the analysis (dependent equation type), analytic test type and dependent variables
int BEMSetupAnalysis(
	CommandLineFilter *cmd,			//The pointer to the command line filter config file
	int numDimensions,				//The number of dimensions in the problem
	int meshType,					//The type of mesh
	int numNodes,					//The number of nodes in the problem
	double **geometry,				//The mesh geometry
	int *analysisType,				//On exit, the type of analysis (equation type)
	int *numVariables,				//On exit, the number of variables per node
	int *numDOFS,					//On exit, the number of degrees-of-freedom (DOFS)
	int *analyticTest,				//On exit, the type of analytic problem (if any)
	double **u,						//On exit, The u values array
	double **q,						//On exit, The q values array
	double **uAnalytic,				//On exit, the analytic u values (if any)
	double **qAnalytic				//On exit, the analytic q values (if any)
	)
{
	// Local variables
	int dofIdx, err, nodeIdx;
	double analyticValue;

	// Check incoming pointers
	if(cmd == NULL) {
		cout << "ERROR: The command line filter is NULL."<<endl;
		BEM_ERROR
	}
	if(*u != NULL) {
		cout << "ERROR: u pointer is not NULL."<<endl;
		BEM_ERROR
	}
	if(*q != NULL) {
		cout << "ERROR: q pointer is not NULL."<<endl;
		BEM_ERROR
	}
	if(*uAnalytic != NULL) {
		cout << "ERROR: uAnalytic pointer is not NULL."<<endl;
		BEM_ERROR
	}
	if(*qAnalytic != NULL) {
		cout << "ERROR: qAnalytic pointer is not NULL."<<endl;
		BEM_ERROR
	}

	*analyticTest = NO_ANALYTIC_TEST;

	// Get the type of analysis
	if(cmd->IsPresent('a')) {
		if(!strcmp(cmd->GetString('a'),"Laplace")) {
			*analysisType = LAPLACE_ANALYSIS_TYPE;
		} else if(!strcmp(cmd->GetString('a'),"LinearElasticity")) {
			*analysisType = LINEAR_ELASTICITY_ANALYSIS_TYPE;
		} else {
			cout << "ERROR: Boundary element analysis equation command string of "<<cmd->GetString('a')<<" is unknown."<<endl;
			BEM_ERROR
		}
	} else {
		*analysisType = LAPLACE_ANALYSIS_TYPE;
	}

	//Set the number of variables
	switch(*analysisType) {
	case LAPLACE_ANALYSIS_TYPE:
		// One scalar variable per node
		*numVariables = 1;
		break;
	case LINEAR_ELASTICITY_ANALYSIS_TYPE:
		// One vector variable per node of size dimensions
		*numVariables = numDimensions;
		break;
	default:
		cout << "ERROR: The analysisType of "<<*analysisType<<" is invalid."<<endl;
		BEM_ERROR
		break;
	}
	*numDOFS = numNodes*(*numVariables);

	// Check if we are doing an analytic test case
	if(cmd->IsPresent('t')){
		switch(*analysisType) {
		case LAPLACE_ANALYSIS_TYPE:
			switch (meshType) {
			case READIN_MESH:
				// Mesh is read in
				cout << "ERROR: Analytic tests not implemented for a read in mesh with Laplace analysis type."<<endl;
				BEM_ERROR
				break;
			case CIRCLE_GENERATED_MESH:
				// Circle generated mesh
				// Check if we are doing an analytic test case
				if(!strcmp(cmd->GetString('t'),"u")) {
					*analyticTest = U_ANALYTIC_TEST;
				} else if(!strcmp(cmd->GetString('t'),"q")) {
					*analyticTest = Q_ANALYTIC_TEST;
				} else {
					cout << "ERROR: Analytic test command string of "<<cmd->GetString('t')<<" is unknown."<<endl;
					BEM_ERROR
				}

				// Allocate the analytic u and q vectors
				*uAnalytic = new double [ *numDOFS ];
				*qAnalytic = new double [ *numDOFS ];
				// Set up the analtyic u and q vectors
				// Analytic solution is u(x,y) = x^2 + 2xy - y^2; q(x,y) = (2x + 4xy - 2y)/sqrt(x^2 + y^2)
				for(nodeIdx = 0; nodeIdx < numNodes; nodeIdx++) {
					dofIdx = nodeIdx;
					err = BEMAnalyticFunction(*analysisType, LAPLACE_2D_ANALTYIC_FUNCTION_1, U_ANALYTIC_VALUE_TYPE, 
							geometry[ nodeIdx ], &analyticValue);
					if(err != ANALYTIC_FUNCTIONS_NO_ERROR) {
						cout << "ERROR: Error code "<<err<<" when calculating analytic function."<<endl;
						BEM_ERROR
					}
					(*uAnalytic)[ dofIdx ] = analyticValue;
					err = BEMAnalyticFunction(*analysisType, LAPLACE_2D_ANALTYIC_FUNCTION_1, Q_ANALYTIC_VALUE_TYPE,
							geometry[ nodeIdx ], &analyticValue);
					if(err != ANALYTIC_FUNCTIONS_NO_ERROR) {
						cout << "ERROR: Error code "<<err<<" when calculating analytic function."<<endl;
						BEM_ERROR
					}
					(*qAnalytic)[ dofIdx ] = analyticValue;
				}
				break;
			case PLATE_GENERATED_MESH:
				// Plate generated mesh
				cout << "ERROR: Analytic tests not implemented for a plate generated mesh with Laplace analysis type."<<endl;
				BEM_ERROR
				break;
			default:
				cout << "ERROR: Mesh type of "<<meshType<<" is not defined."<<endl;
				BEM_ERROR
				break;
			}
			break;
		case LINEAR_ELASTICITY_ANALYSIS_TYPE:
			cout << "ERROR: Analytic tests not implemented for linear elasticity analysis type."<<endl;
			BEM_ERROR
			break;
		default:
			cout << "ERROR: The analysisType of "<<*analysisType<<" is invalid."<<endl;
			BEM_ERROR
			break;
		}
	}

	// Allocate and initialise BEM variable arrays		
	*u = new double [ *numDOFS ];
	*q = new double [ *numDOFS ];
	for(dofIdx = 0; dofIdx < *numDOFS; dofIdx++) {
		(*u)[ dofIdx ] = 0.0;
		(*q)[ dofIdx ] = 0.0;
	}

	return BEM_NO_ERROR;
}

// Finalise the Analysis and deallocate any variables.
int BEMFinaliseAnalysis(
	double **u,						//The u values array to finalise
	double **q,						//The q values array to finalise
	double **uAnalytic,				//The analytic u values (if any) to finalise
	double **qAnalytic				//The analytic q values (if any) to finalise
	)
{
	if(*u != NULL) delete [] *u;
	if(*q != NULL) delete [] *q;
	if(*uAnalytic != NULL) delete [] *uAnalytic;
	if(*qAnalytic != NULL) delete [] *qAnalytic;

	return BEM_NO_ERROR;
}

// Setup the boundary conditions for the problem
int BEMSetupBoundaryConditions(
	CommandLineFilter *cmd,			//The pointer to the command line filter config file
	int numDimensions,				//The number of dimensions in the problem
	int meshType,					//The type of mesh
	int numNodes,					//The number of nodes in the problem
	int analysisType,				//The analysis type of the problem
	int numVariables,				//The number of variables per node in the problem
	int numDOFS,					//The number of degrees-of-freedom (DOFS) in the problem
	int analyticTest,				//The analytic test type for the problem (if any)
	double *u,						//The u values array for the problem
	double *q,						//The q values array for the problem
	double *uAnalytic,				//The analytic u values array (if any)
	double *qAnalytic,				//The analytic q values array (if any)
	int ***nodeBoundaryConditions	//On exit, the nodal boundary condition types
	)
{
	// Local variables	
	int bcNode,boundaryConditionIdx,dofIdx,nodeIdx,rowIdx,variableIdx;
	int numEssentialBoundaryConditions,numNaturalBoundaryConditions;
	int *bcENode = NULL, *bcNNode = NULL, *bcEVar = NULL, *bcNVar = NULL, bcVar;
	double bcValue;
	double *bcEValue = NULL,*bcNValue = NULL;
	ifstream ifid;

	// Check incoming pointers
	if(cmd == NULL) {
		cout << "ERROR: The command line filter is NULL."<<endl;
		BEM_ERROR
	}

	if(*nodeBoundaryConditions != NULL) {
		cout << "ERROR: nodeBoundaryConditions pointer is not NULL."<<endl;
		BEM_ERROR
	}

	// Check if we are doing an analytic test case

	if(analyticTest != NO_ANALYTIC_TEST) {
		switch (analysisType) {
		case LAPLACE_ANALYSIS_TYPE:
			switch (analyticTest) {
			case U_ANALYTIC_TEST:
				numEssentialBoundaryConditions = numNodes;
				numNaturalBoundaryConditions = 0;
				break;
			case Q_ANALYTIC_TEST:
				numEssentialBoundaryConditions = 1;
				numNaturalBoundaryConditions = numNodes - 1;
				break;
			default:
				cout << "ERROR: analyticTest of "<<analyticTest<<" is not defined."<<endl;
				BEM_ERROR
				break;
			}

			//Allocating space for boundary condition arrays.
			if(numEssentialBoundaryConditions > 0) {
				bcENode = new int [ numEssentialBoundaryConditions ];
				bcEVar= new int [ numEssentialBoundaryConditions ];
				bcEValue = new double [ numEssentialBoundaryConditions ];
			}
			if(numNaturalBoundaryConditions > 0) {
				bcNNode = new int [ numNaturalBoundaryConditions ];
				bcNVar = new int [ numNaturalBoundaryConditions ];
				bcNValue = new double [ numNaturalBoundaryConditions ];
			}
			//Set the analytic values as BCs
			for(nodeIdx = 0; nodeIdx < numNodes; nodeIdx++) {
				dofIdx = nodeIdx;
				switch (analyticTest) {
				case U_ANALYTIC_TEST:
					bcENode[ nodeIdx ] = nodeIdx;
					bcEVar[ nodeIdx ] = 0;
					bcEValue[ nodeIdx ] = uAnalytic[ dofIdx ];
					break;
				case Q_ANALYTIC_TEST:
					if(nodeIdx == 0) {
						bcENode[ 0 ] = 0;
						bcEVar[ 0 ] = 0;
						bcEValue[ 0 ] = uAnalytic[ 0 ];
					} else {
						bcNNode[ nodeIdx - 1 ] = nodeIdx;
						bcNVar[ nodeIdx - 1 ] = 0;
						bcNValue[ nodeIdx - 1 ] = qAnalytic[ dofIdx ];
					}
					break;
				default:
					cout << "ERROR: analyticTest of "<<analyticTest<<" is not defined."<<endl;
					BEM_ERROR
					break;
				}
			}
			break;
		case LINEAR_ELASTICITY_ANALYSIS_TYPE:
			cout << "ERROR: Analytic tests not implemented for linear elasticity analysis type."<<endl;
			BEM_ERROR
			break;
		default:
			cout << "ERROR: The analysisType of "<<analysisType<<" is invalid."<<endl;
			BEM_ERROR
			break;
		}
	} else {

		// Reading in the essential BC information:
		if(cmd->IsPresent('b')) {
			ifid.open(cmd->GetString('b'));
			ifid >> numEssentialBoundaryConditions;
			// Check input
			if(numEssentialBoundaryConditions >= 0 && numEssentialBoundaryConditions < numDOFS) {
				// Allocating space for the essential boundary conditions:
				bcENode = new int [ numEssentialBoundaryConditions ];
				bcEVar = new int [ numEssentialBoundaryConditions ];
				bcEValue = new double [ numEssentialBoundaryConditions ];
				for(boundaryConditionIdx = 0; boundaryConditionIdx < numEssentialBoundaryConditions; boundaryConditionIdx++) {
					if(numVariables == 1) {
						bcVar = 0;
						ifid >>bcNode>>bcValue;
					} else {
						ifid >>bcNode>>bcVar>>bcValue;
					}
					// Check input
					if(bcNode >= 0 && bcNode < numNodes) {
						if(bcVar >= 0 && bcVar < numVariables) {
							bcENode[ boundaryConditionIdx ] = bcNode;
							bcEVar[ boundaryConditionIdx ] = bcVar;
							bcEValue[ boundaryConditionIdx ] = bcValue;
						} else {
							cout << "ERROR: The essential boundary condition variable number of "<<bcVar<<" for essential boundary condition number "<<boundaryConditionIdx<<" is invalid. The number must be >= 0 and <"<<numVariables<<"."<<endl;
							BEM_ERROR
						}
					} else {
						cout << "ERROR: The essential boundary condition node number of "<<bcNode<<" for essential boundary condition number "<<boundaryConditionIdx<<" is invalid. The number must be >= 0 and <"<<numNodes<<"."<<endl;
						BEM_ERROR
					}
				}
			} else {
				cout << "ERROR: The number of essential boundary conditions of "<<numEssentialBoundaryConditions<<" is invalid. The number must be >= 0 and <"<<numDOFS<<endl;
				BEM_ERROR
			}
			ifid.close();
		} else {
			cout << "ERROR: essential BC command line is not present."<<endl;
			BEM_ERROR
		}
		// Reading in the natural BC information:
		if(cmd->IsPresent('v')) {
			ifid.open(cmd->GetString('v'));
			ifid >> numNaturalBoundaryConditions;
			if(numNaturalBoundaryConditions >= 0 && numNaturalBoundaryConditions < numDOFS) {
				// Allocating space for the essential boundary conditions:
				bcNNode = new int [ numNaturalBoundaryConditions ];
				bcNVar = new int [ numNaturalBoundaryConditions ];
				bcNValue = new double [ numNaturalBoundaryConditions ];
				for(boundaryConditionIdx = 0; boundaryConditionIdx < numNaturalBoundaryConditions; boundaryConditionIdx++) {
					if(numVariables == 1) {
						bcVar = 0;
						ifid >>bcNode>>bcValue;
					} else {
						ifid >>bcNode>>bcVar>>bcValue;
					}	
					// Check input
					if(bcNode >= 0 && bcNode < numNodes) {
						if(bcVar >= 0 && bcVar < numVariables) {
							bcNNode[ boundaryConditionIdx ] = bcNode;
							bcNVar[ boundaryConditionIdx ] = bcVar;
							bcNValue[ boundaryConditionIdx ] = bcValue;
						} else {
							cout << "ERROR: The natural boundary condition variable number of "<<bcVar<<" for natural boundary condition number "<<boundaryConditionIdx<<" is invalid. The number must be >= 0 and <"<<numVariables<<"."<<endl;
							BEM_ERROR
						}
					} else {
						cout << "ERROR: The natural boundary condition node number of "<<bcNode<<" for natural boundary condition number "<<boundaryConditionIdx<<" is invalid. The number must be >= 0 and <"<<numNodes<<"."<<endl;
						BEM_ERROR
					}
				}
			} else {
				cout << "ERROR: The number of natural boundary conditions of "<<numNaturalBoundaryConditions<<" is invalid. The number must be >= 0 and <"<<numDOFS<<endl;
				BEM_ERROR
			}
			ifid.close();
		} else {
			cout << "ERROR: natural BC command line is not present."<<endl;
			BEM_ERROR
		}
	}
	// Finish BC setup

	// Allocate BEM BC arrys 
	*nodeBoundaryConditions = new int* [ numNodes ];
	for(nodeIdx = 0; nodeIdx < numNodes; nodeIdx++) {
		(*nodeBoundaryConditions)[ nodeIdx ] = new int [ numVariables ];
	}
	for(nodeIdx = 0; nodeIdx < numNodes; nodeIdx++) {
		for(variableIdx = 0; variableIdx < numVariables; variableIdx++) {
			(*nodeBoundaryConditions)[ nodeIdx ][ variableIdx ] = NODE_NO_BC;
		}
	}

	// Set node essential BCs
	for(boundaryConditionIdx = 0; boundaryConditionIdx < numEssentialBoundaryConditions; boundaryConditionIdx++) {
		if((*nodeBoundaryConditions)[ bcENode[ boundaryConditionIdx ] ][ bcEVar[ boundaryConditionIdx ] ] == NODE_NO_BC) {
			(*nodeBoundaryConditions)[ bcENode[ boundaryConditionIdx ] ][ bcEVar[ boundaryConditionIdx ] ] = NODE_ESSENTIAL_BC;
			rowIdx = bcENode[ boundaryConditionIdx ]*numVariables + bcEVar[ boundaryConditionIdx ];
			u[ rowIdx ] = bcEValue[ boundaryConditionIdx ];
		} else {
			cout << "ERROR: cannot set essential BC for node "<<bcENode[ boundaryConditionIdx ]<<" as it already has a BC set."<<endl;
			BEM_ERROR
		}
	}
	// Set node natural BCs
	for(boundaryConditionIdx = 0; boundaryConditionIdx < numNaturalBoundaryConditions; boundaryConditionIdx++) {
		if((*nodeBoundaryConditions)[ bcNNode[ boundaryConditionIdx ] ][ bcNVar[ boundaryConditionIdx ] ] == NODE_NO_BC) {
			(*nodeBoundaryConditions)[ bcNNode[ boundaryConditionIdx ] ][ bcNVar[ boundaryConditionIdx ] ] = NODE_NATURAL_BC;
			rowIdx = bcNNode[ boundaryConditionIdx ]*numVariables + bcNVar[ boundaryConditionIdx ];
			q[ rowIdx ] = bcNValue[ boundaryConditionIdx ];
		} else {
			cout << "ERROR: cannot set natural BC for node "<<bcNNode[ boundaryConditionIdx ]<<" as it already has a BC set."<<endl;
			BEM_ERROR
		}
	}
	// Check we have a BC at each node and variable
	for(nodeIdx = 0; nodeIdx < numNodes; nodeIdx++) {
		for(variableIdx = 0; variableIdx < numVariables; variableIdx++) {
			if((*nodeBoundaryConditions)[ nodeIdx ][ variableIdx ] == NODE_NO_BC) {
				cout << "ERROR: Node "<<nodeIdx<<", variable "<<variableIdx<<" does not have any BCs set."<<endl;
				BEM_ERROR
			}
		}
	}

	if(bcENode != NULL) delete [] bcENode;
	if(bcEVar != NULL) delete [] bcEVar;
	if(bcEValue != NULL) delete [] bcEValue;
	
	if(bcNNode != NULL)	delete [] bcNNode;
	if(bcNVar != NULL) delete [] bcNVar;
	if(bcNValue != NULL) delete [] bcNValue;

	return BEM_NO_ERROR;
}

// Finalise the Boundary Conditions and deallocate any variables.
int BEMFinaliseBoundaryConditions(
	const int numNodes,					//The number of nodes in the problem
	int ***nodeBoundaryConditions		//The nodal boundary condition types to finalise
	)
{
	// Local variables
	int nodeIdx;

	if(*nodeBoundaryConditions != NULL) {
		for(nodeIdx = 0; nodeIdx < numNodes; nodeIdx++) {
			if((*nodeBoundaryConditions)[ nodeIdx ]) delete [] (*nodeBoundaryConditions)[ nodeIdx ];
		}
		delete [] *nodeBoundaryConditions;
	}

	return BEM_NO_ERROR;
}

// Setup the various arrays and vectors required for the BEM solution
int BEMSetupArrays(
	const int numDimensions,		//The number of dimensions in the problem 
	const int elementType,			//The BEM element type
	const int numDOFS,				//The number of degrees-of-freedom (DOFS) in the problem
	double ***elementDOFS,			//On exit, the temporary element DOFS storage array
	double ***A,					//On exit, the BEM A matrix
	double ***B,					//On exit, the BEM B matrix
	double ***C,					//On exit, the BEM C matrix
	double ***origC,				//On exit, the original C matrix
	double **x,						//On exit, the solution vector
	double **f						//On exit, the RHS vector
	)
{
	// Local variables
	int columnIdx,coordinateIdx,localNodeIdx,rowIdx;

	// Check incoming pointers
	if(*elementDOFS != NULL) {
		cout << "ERROR: elementDOFS pointer is not NULL."<<endl;
		BEM_ERROR
	}
	if(*A != NULL) {
		cout << "ERROR: A pointer is not NULL."<<endl;
		BEM_ERROR
	}
	if(*B != NULL) {
		cout << "ERROR: B pointer is not NULL."<<endl;
		BEM_ERROR
	}
	if(*C != NULL) {
		cout << "ERROR: C pointer is not NULL."<<endl;
		BEM_ERROR
	}
	if(*origC != NULL) {
		cout << "ERROR: origC pointer is not NULL."<<endl;
		BEM_ERROR
	}
	if(*x != NULL) {
		cout << "ERROR: x pointer is not NULL."<<endl;
		BEM_ERROR
	}
	if(*f != NULL) {
		cout << "ERROR: f pointer is not NULL."<<endl;
		BEM_ERROR
	}

	//Allocate and initialise variables
	*elementDOFS = new double* [ numDimensions ];
	for(coordinateIdx = 0; coordinateIdx < numDimensions; coordinateIdx++) {
		(*elementDOFS)[ coordinateIdx ] = new double [ elementType + 1 ];
		for(localNodeIdx = 0; localNodeIdx < elementType + 1; localNodeIdx++) {
			(*elementDOFS)[ coordinateIdx ][ localNodeIdx ] = 0.0;
		}
	}
	*A = new double* [ numDOFS ];
	*B = new double* [ numDOFS ];
	*C = new double* [ numDOFS ];
	*origC = new double* [ numDOFS ];
	*x = new double [ numDOFS ];
	*f = new double [ numDOFS];
	for(rowIdx = 0; rowIdx < numDOFS; rowIdx++) {
		(*A)[ rowIdx ] = new double [ numDOFS ];
		(*B)[ rowIdx ] = new double [ numDOFS ];
		(*C)[ rowIdx ] = new double [ numDOFS ];
		(*origC)[ rowIdx ] = new double [ numDOFS ];
		for(columnIdx = 0; columnIdx < numDOFS; columnIdx++) {
			(*A)[ rowIdx ][ columnIdx ] = 0.0;
			(*B)[ rowIdx ][ columnIdx ] = 0.0;
			(*C)[ rowIdx ][ columnIdx ] = 0.0;
			(*origC)[ rowIdx ][ columnIdx ] = 0.0;
		}
		(*x)[ rowIdx ] = 0.0;
		(*f)[ rowIdx ] = 0.0;
	}

	return BEM_NO_ERROR;
}

// Finalise the BEM arrays and vectors and deallocate any variables.
int BEMFinaliseArrays(
	const int numDimensions,		//The number of dimensions in the problem
	const int numDOFS,				//The number of degrees-of-freedom (DOFS) in the problem
	double ***elementDOFS,			//The temporary storage for element degrees-of-freedom to finalise
	double ***A,					//The BEM A matrix to finalise
	double ***B,					//The BEM B matrix to finalise
	double ***C,					//The linear solution C matrix to finalise	
	double ***origC,				//The original C matrix to finalise
	double **x,						//The solution vector to finalise
	double **f						//The RHS vector to finalise
	)
{
	// Local variables
	int coordinateIdx,rowIdx;

	if(*elementDOFS != NULL) {
		for(coordinateIdx = 0; coordinateIdx < numDimensions; coordinateIdx++) {
			if((*elementDOFS)[ coordinateIdx ] != NULL) delete [] (*elementDOFS)[ coordinateIdx ];
		}
		delete [] *elementDOFS;
	}
	if(*A != NULL) {
		for(rowIdx = 0; rowIdx < numDOFS; rowIdx++) {
			if((*A)[ rowIdx ] != NULL) delete [] (*A)[ rowIdx ];
		}
		delete [] *A;
	}
	if(*B != NULL) {
		for(rowIdx = 0; rowIdx < numDOFS; rowIdx++) {
			if((*B)[ rowIdx ] != NULL) delete [] (*B)[ rowIdx ];
		}
		delete [] *B;
	}
	if(*C != NULL) {
		for(rowIdx = 0; rowIdx < numDOFS; rowIdx++) {
			if((*C)[ rowIdx ] != NULL) delete [] (*C)[ rowIdx ];
		}
		delete [] *C;
	}
	if(*origC != NULL) {
		for(rowIdx = 0; rowIdx < numDOFS; rowIdx++) {
			if((*origC)[ rowIdx ] != NULL) delete [] (*origC)[ rowIdx ];
		}
		delete [] *origC;
	}
	if(*x != NULL) delete [] *x;
	if(*f != NULL) delete [] *f;

	return BEM_NO_ERROR;
}
