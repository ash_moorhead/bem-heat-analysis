#ifndef BEM_ANALYSISANDOUTPUT

#include <stdlib.h>
#include <fstream>
#include <iostream>
#include <math.h>

using namespace std; 

// Output to a spreadsheet the comparison of the analytic solution with the numerical solution
int BEMAnalyticAnalysis(
	char *analysisFilename,					// The name of the spreadsheet
	int elementType,						// The type of BEM element used in the mesh. See BEM_BasisFunctions.h for valid element types.
	int numNodes,							// The number of nodes in the mesh.
	int numElements,						// The number of elements in the mesh.
	int numVariables,						// The number of variables at each node.
	int numDOFS,							// The number of degrees-of-freedom (DOFS) in the problem.
	double averageElementSize,				// The average element size in the mesh.
	double *u,								// u[ rowDofIdx ]. The value of the numerically calculated rowDofIdx'th u value.
	double *q,								// q[ rowDofIdx ]. The value of the numerically calculated rowDofIdx'th q value.
	double *uAnalytic,						// uAnalytic[ rowDofIdx ]. The value of the analytic rowDofIdx'th u value.
	double *qAnalytic						// qAnalytic[ rowDofIdx ]. The value of the analytic rowDofIdx'th q value.
	);

// Output to a spreadsheet the linear systems in the problem i.e., A u = B q and C x = f.
int BEMLinearSystemsOutput(
	char *linearSystemFilename,					// The name of the spreadsheet.
	int numDOFS,								// The number of degrees-of-freedom in the problem.
	double *u,									// u[ rowDofIdx ]. The rowDofIdx'th DOF u value.
	double *q,									// q[ rowDofIdx ]. The rowDofIdx'th DOF q value.
	double **A,									// A[ rowDofIdx ][ columnDofIdx ]. The (rowDofIdx'th, columnDofIdx'th) A matrix coefficient.
	double **B,									// B[ rowDofIdx ][ columnDofIdx ]. The (rowDofIdx'th, columnDofIdx'th) B matrix coefficient.
	double **C,									// C[ rowDofIdx ][ columnDofIdx ]. The (rowDofIdx'th, columnDofIdx'th) C matrix coefficient.
	double *x,									// x[ rowDofIdx ]. The rowDofIdx'th DOF solution vector value.
	double *f									// x[ rowDofIdx ]. The rowDofIdx'th DOF RHS vector value.
	);

// Output to a spreadsheet the numerical solution .
int BEMResultsOutput(
	char *resultsFilename,					// The name of the spreadsheet.
	int numNodes,							// The number of nodes in the mesh.
	int numVariables,						// The number of variables at each node.
	double *u,								// u[ rowDofIdx ]. The rowDofIdx'th DOF u value.				
	double *q								// q[ rowDofIdx ]. The rowDofIdx'th DOF q value.	
	);

#define BEM_ANALYSISANDOUTPUT
#endif