#ifndef BEM_SETUP

#include <stdlib.h>
#include "CommandLineFilter.h"

using namespace std;

// Setup the BEM problem
int BEMSetupProblem(
	CommandLineFilter *cmd,			//The pointer to the command line filter config file
	int *numDimensions				//On exit, the number of dimensions of the problem
	);

// Finalise the BEM Problem and deallocate any variables.
int BEMFinaliseProblem(
	);

// Setup the mesh and geometric information
int BEMSetupGeometry(
	CommandLineFilter *cmd,			//The pointer to the command line filter config file
	int numDimensions,				//The number of dimensions of the problem
	int *meshType,					//On exit, the type of mesh
	int *elementType,				//On exit, the type of boundary element
	int *numNodes,					//On exit, the number of nodes in the problem
	int *numElements,				//On exit, the number of elements in the problem
	int *nodesPerElement,			//On exit, the number of nodes per element
	int ***elementNodes,			//On exit, the nodes in each element
	int **nodeCorners,				//On exit, the nodes corner array
	double ***geometry,				//On exit, the nodal geometry
	double *averageElementSize		//On exit, the average element size
	);

// Finalise the Geometry and deallocate any variables.
int BEMFinaliseGeometry(
	int numNodes,					//The number of nodes in the problem
	int numElements,				//The number of nodes in the problem
	int ***elementNodes,			//The nodes in each element array to finalise
	int **nodeCorners,				//The nodes corner array to finalise
	double ***geometry				//The nodal geometry to finalise
	);

// Setup the analysis (dependent equation type), analytic test type and dependent variables
int BEMSetupAnalysis(
	CommandLineFilter *cmd,			//The pointer to the command line filter config file
	int numDimensions,				//The number of dimensions in the problem
	int meshType,					//The type of mesh
	int numNodes,					//The number of nodes in the problem
	double **geometry,				//The mesh geometry
	int *analysisType,				//On exit, the type of analysis (equation type)
	int *numVariables,				//On exit, the number of variables per node
	int *numDOFS,					//On exit, the number of degrees-of-freedom (DOFS)
	int *analyticTest,				//On exit, the type of analytic problem (if any)
	double **u,						//On exit, The u values array
	double **q,						//On exit, The q values array
	double **uAnalytic,				//On exit, the analytic u values (if any)
	double **qAnalytic				//On exit, the analytic q values (if any)
	);

// Finalise the Analysis and deallocate any variables.
int BEMFinaliseAnalysis(
	double **u,						//The u values array to finalise
	double **q,						//The q values array to finalise
	double **uAnalytic,				//The analytic u values (if any) to finalise
	double **qAnalytic				//The analytic q values (if any) to finalise
	);

// Setup the boundary conditions for the problem
int BEMSetupBoundaryConditions(
	CommandLineFilter *cmd,			//The pointer to the command line filter config file
	int numDimensions,				//The number of dimensions in the problem
	int meshType,					//The type of mesh
	int numNodes,					//The number of nodes in the problem
	int analysisType,				//The analysis type of the problem
	int numVariables,				//The number of variables per node in the problem
	int numDOFS,					//The number of degrees-of-freedom (DOFS) in the problem
	int analyticTest,				//The analytic test type for the problem (if any)
	double *u,						//The u values array for the problem
	double *q,						//The q values array for the problem
	double *uAnalytic,				//The analytic u values array (if any)
	double *qAnalytic,				//The analytic q values array (if any)
	int ***nodeBoundaryConditions	//On exit, the nodal boundary condition types
	);

// Finalise the Boundary Conditions and deallocate any variables.
int BEMFinaliseBoundaryConditions(
	const int numNodes,					//The number of nodes in the problem
	int ***nodeBoundaryConditions		//The nodal boundary condition types to finalise
	);

// Setup the various arrays and vectors required for the BEM solution
int BEMSetupArrays(
	const int numDimensions,		//The number of dimensions in the problem 
	const int elementType,			//The BEM element type
	const int numDOFS,				//The number of degrees-of-freedom (DOFS) in the problem
	double ***elementDOFS,			//On exit, the temporary element DOFS storage array
	double ***A,					//On exit, the BEM A matrix
	double ***B,					//On exit, the BEM B matrix
	double ***C,					//On exit, the BEM C matrix
	double ***origC,				//On exit, the original C matrix
	double **x,						//On exit, the solution vector
	double **f						//On exit, the RHS vector
	);

// Finalise the BEM arrays and vectors and deallocate any variables.
int BEMFinaliseArrays(
	const int numDimensions,		//The number of dimensions in the problem
	const int numDOFS,				//The number of degrees-of-freedom (DOFS) in the problem
	double ***elementDOFS,			//The temporary storage for element degrees-of-freedom to finalise
	double ***A,					//The BEM A matrix to finalise
	double ***B,					//The BEM B matrix to finalise
	double ***C,					//The linear solution C matrix to finalise	
	double ***origC,				//The original C matrix to finalise
	double **x,						//The solution vector to finalise
	double **f						//The RHS vector to finalise
	);

#define BEM_SETUP
#endif
