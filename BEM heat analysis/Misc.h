#ifndef MISC
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

template <class T>
std::string to_string(T t, std::ios_base & (*f)(std::ios_base&))
{
  std::ostringstream oss;
  oss << f << t;
  return oss.str();
}

#ifndef PI
#define PI 3.14159265358979323846264338327950288
#endif

#ifndef PAUSE
#define PAUSE {char pause; cout << "Paused.."<<endl; cin >> pause;}
#endif

#define VERBOSE 1

#ifndef TAB
#define TAB "\t"
#endif

#ifndef TRACECLASS

class TraceClass
{
public:
	TraceClass(char *in){
		strcpy_s(tracename,in);
		cout << tracename << endl;
	};
	~TraceClass(){
		cout << tracename << " returned"<<endl;
	};
	char tracename[100];
};

#if VERBOSE
#define trace(tracename) TraceClass temptrace(tracename);
#else
#define trace(tracename) {}
#endif
#define TRACECLASS
#endif

#ifndef myDelete
#define myDelete(array) if(array!=NULL){delete [] array; array=NULL;}
#endif

#ifndef myDestroy
#define myDestroy(array) if(array!=NULL){array->Destroy(); delete array; array=NULL;}
#endif



// If running in Linux, remove this line:
//double cbrt(double x);
void user_wait();
void Sort(int *sortme, int &n);
void GetOrder(int *sortme, double *sortbyme, int n);
void ReOrder(int *sortbyme, double *sortme, int n);
void GetOrder(int *sortme, int *sortbyme, int n);
void ReOrder(int *sortbyme, int *sortme, int n);
void MoveToPoint(int n, double *array, double offset);
double Power(double num, int power);
double Det2x2(double a, double b, double c, double d);
void Transpose(double **a, double **at, int row, int col);
void Zero(int num, int *v);
void Zero(int num, double *v);






#define MISC
#endif

