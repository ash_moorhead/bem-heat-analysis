#ifndef BEM_BASISFUNCTIONS

#include <stdlib.h>

using namespace std;

// Valid basis function element types
#define BASIS_FUNCTIONS_LINEAR_LAGRANGE_ELEMENT_TYPE 1			// Linear Lagrange basis function
#define BASIS_FUNCTIONS_QUADRATIC_LAGRANGE_ELEMENT_TYPE 2		// Quadratic Lagrange basis function

// Valid basis function partial derivative types
#define BASIS_FUNCTIONS_NO_PARTIAL_DERIVATIVE 0					// No partial derivative of the basis function
#define BASIS_FUNCTIONS_FIRST_PARTIAL_DERIVATIVE 1				// First partial derivative of the basis function i.e., del Phi/ del xi
#define BASIS_FUNCTIONS_SECOND_PARTIAL_DERIVATIVE 2				// Second partial derivative of the basis function i.e., del^2 Phi/ del xi^2

// Valid basis function errors
#define BASIS_FUNCTIONS_NO_ERROR 0								// No error
#define BASIS_FUNCTIONS_INVALID_ELEMENT_TYPE 1					// The element type is not valid. See above for valid element types.
#define BASIS_FUNCTIONS_INVALID_LOCAL_NODE_IDX_ERROR 2			// The local node index is not valid. 
#define BASIS_FUNCTIONS_INVALID_DERIVATIVE_IDX_ERROR 3			// The partial derivative type is not valid. See above for valid partial derivative types.

// Evaluate a particular basis function at at particular xi location
int BasisFunctionEvaluateXi(
		const int elementType,				// The element type to evaluate the basis function for. See above for valid element types.
		const int localNodeIdx,				// The local node index which identifies the particular basis function.
		const int derivativeIdx,			// The partial derivative index of the basis funtion to evaluate. See above for valid partial derivative indices.
		const double xi,					// The xi location to evaluate the basis function at.
		double* phi							// On exit, the value of the evaluated basis function.
		);

// Interpolate an elemental basis function at at particular xi location
int BasisFunctionInterpolateXi(
	const int elementType, 					// The element type to interpolate the basis function for. See above for valid element types.
	const int derivativeIdx,				// The partial derivative index of the funtion to interpolate. See above for valid partial derivative indices.
	const double xi,						// The xi location to interpolate the function at.
	const double* dofValues,				// dofValues[ localDofIdx ]. The element degrees-of-freedom for the interpolation.
	double* phi								// On exit, the value of the interpolated function.
	);

#define BEM_BASISFUNCTIONS
#endif
