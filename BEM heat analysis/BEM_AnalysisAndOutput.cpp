/*
	This file contains routines for the analysis and output of BEM results
*/

#include "BEM_AnalysisAndOutput.h"
#include "BEM_Constants.h"
#include "Constants.h"
#include "Misc.h"

// Output to a spreadsheet the comparison of the analytic solution with the numerical solution
int BEMAnalyticAnalysis(
	char *analysisFilename,					// The name of the spreadsheet
	int elementType,						// The type of BEM element used in the mesh. See BEM_BasisFunctions.h for valid element types.
	int numNodes,							// The number of nodes in the mesh.
	int numElements,						// The number of elements in the mesh.
	int numVariables,						// The number of variables at each node.
	int numDOFS,							// The number of degrees-of-freedom (DOFS) in the problem.
	double averageElementSize,				// The average element size in the mesh.
	double *u,								// u[ rowDofIdx ]. The value of the numerically calculated rowDofIdx'th u value.
	double *q,								// q[ rowDofIdx ]. The value of the numerically calculated rowDofIdx'th q value.
	double *uAnalytic,						// uAnalytic[ rowDofIdx ]. The value of the analytic rowDofIdx'th u value.
	double *qAnalytic						// qAnalytic[ rowDofIdx ]. The value of the analytic rowDofIdx'th q value.
	)
{
	// Local variables
	int rowDofIdx, nodeIdx, variableIdx;
	double uAbsError,uPercentError,uRMSError,qAbsError,qPercentError,qRMSError;
	ofstream ofid;

	ofid.open(analysisFilename);
	// Output header information
	ofid <<"Element type"<<TAB<<elementType<<endl;
	ofid <<"Number of elements"<<TAB<<numElements<<endl;
	ofid <<"Average element size"<<TAB<<averageElementSize<<endl;
	ofid <<endl;
	if(numVariables == 1) {
		ofid <<"Node"<<TAB<<"u"<<TAB<<"analytic u"<<TAB<<"q"<<TAB<<"analytic q"<<TAB<<"|uError|"<<TAB<<"|qError|"<<TAB<<"% uError"<<TAB<<"% qError"<<endl;
	} else {
		ofid <<"Node"<<TAB<<"Variable"<<TAB<<"u"<<TAB<<"analytic u"<<TAB<<"q"<<TAB<<"analytic q"<<TAB<<"|uError|"<<TAB<<"|qError|"<<TAB<<"% uError"<<TAB<<"% qError"<<endl;
	}
	//Comupute RMS errors in the solution
	uRMSError = 0.0;
	qRMSError = 0.0;
	for(nodeIdx=0; nodeIdx < numNodes; nodeIdx++) {
		for(variableIdx = 0; variableIdx < numVariables; variableIdx++) {
			rowDofIdx = nodeIdx*numVariables + variableIdx;
			// Calcuate the error in the solution
			uAbsError = abs(u[ rowDofIdx ]-uAnalytic[ rowDofIdx ]);
			qAbsError = abs(q[ rowDofIdx ]-qAnalytic[ rowDofIdx ]);
			if(abs(uAnalytic[ rowDofIdx ])>ZERO_TOLERANCE) {
				uPercentError = uAbsError/uAnalytic[ rowDofIdx ];
			} else {
				uPercentError = 0.0;
			}
			if(abs(qAnalytic[ rowDofIdx ])>ZERO_TOLERANCE) {
				qPercentError = qAbsError/qAnalytic[ rowDofIdx ];
			} else {
				qPercentError = 0.0;
			}
			uRMSError += uAbsError*uAbsError;
			qRMSError += qAbsError*qAbsError;
			// Output values and errors
			if(numVariables == 1) {
				ofid <<nodeIdx<<TAB<<u[ rowDofIdx ]<<TAB<<uAnalytic[ rowDofIdx ]<<TAB<<q[ rowDofIdx ]<<TAB<<qAnalytic[ rowDofIdx ]<<TAB<<uAbsError<<TAB<<qAbsError<<TAB<<uPercentError<<TAB<<qPercentError<<endl;
			} else {
				ofid <<nodeIdx<<TAB<<variableIdx<<TAB<<u[ rowDofIdx ]<<TAB<<uAnalytic[ rowDofIdx ]<<TAB<<q[ rowDofIdx ]<<TAB<<qAnalytic[ rowDofIdx ]<<TAB<<uAbsError<<TAB<<qAbsError<<TAB<<uPercentError<<TAB<<qPercentError<<endl;
			}
		}
	}
	ofid <<endl;
	// Output RMS errors
	uRMSError = sqrt(uRMSError/numDOFS);
	qRMSError = sqrt(qRMSError/numDOFS);
	ofid <<"u RMS error"<<TAB<<uRMSError<<endl;
	ofid <<"q RMS error"<<TAB<<qRMSError<<endl;

	ofid.close();

	return BEM_NO_ERROR;
}

// Output to a spreadsheet the linear systems in the problem i.e., A u = B q and C x = f.
int BEMLinearSystemsOutput(
	char *linearSystemFilename,					// The name of the spreadsheet.
	int numDOFS,								// The number of degrees-of-freedom in the problem.
	double *u,									// u[ rowDofIdx ]. The rowDofIdx'th DOF u value.
	double *q,									// q[ rowDofIdx ]. The rowDofIdx'th DOF q value.
	double **A,									// A[ rowDofIdx ][ columnDofIdx ]. The (rowDofIdx'th, columnDofIdx'th) A matrix coefficient.
	double **B,									// B[ rowDofIdx ][ columnDofIdx ]. The (rowDofIdx'th, columnDofIdx'th) B matrix coefficient.
	double **C,									// C[ rowDofIdx ][ columnDofIdx ]. The (rowDofIdx'th, columnDofIdx'th) C matrix coefficient.
	double *x,									// x[ rowDofIdx ]. The rowDofIdx'th DOF solution vector value.
	double *f									// x[ rowDofIdx ]. The rowDofIdx'th DOF RHS vector value.
	)
{
	// Local variables
	int columnDofIdx,rowDofIdx;
	ofstream ofid;
	
	ofid.open(linearSystemFilename);
	// Output A u = B q
	for(rowDofIdx = 0; rowDofIdx < numDOFS; rowDofIdx++) {
		for(columnDofIdx = 0; columnDofIdx < numDOFS; columnDofIdx++) {
			// Output A matrix
			ofid <<A[ rowDofIdx ][ columnDofIdx ]<<TAB;
		}
		// Output u vector
		ofid <<TAB<<u[ rowDofIdx ]<<TAB<<TAB ;
		for(columnDofIdx = 0; columnDofIdx < numDOFS; columnDofIdx++) {
			// Output B matrix
			ofid <<B[ rowDofIdx ][ columnDofIdx ]<<TAB;
		}
		// Output q vector
		ofid <<TAB<<q[ rowDofIdx ]<<TAB<<endl;
	}
	ofid <<endl;
	// Output C x = f
	for(rowDofIdx = 0; rowDofIdx < numDOFS; rowDofIdx++) {
		for(columnDofIdx = 0; columnDofIdx < numDOFS; columnDofIdx++) {
			// Output C matrix
			ofid <<C[ rowDofIdx ][ columnDofIdx ]<<TAB;
		}
		// Output x and f vectors
		ofid <<TAB<<x[ rowDofIdx ]<<TAB<<TAB<<f[ rowDofIdx]<<endl;
	}
	ofid.close();

	return BEM_NO_ERROR;
}

// Output to a spreadsheet the numerical solution .
int BEMResultsOutput(
	char *resultsFilename,					// The name of the spreadsheet.
	int numNodes,							// The number of nodes in the mesh.
	int numVariables,						// The number of variables at each node.
	double *u,								// u[ rowDofIdx ]. The rowDofIdx'th DOF u value.				
	double *q								// q[ rowDofIdx ]. The rowDofIdx'th DOF q value.	
	)
{
	// Local variables
	int nodeIdx, rowDofIdx, variableIdx;
	ofstream ofid;

	ofid.open(resultsFilename);
	// Output header information
	if(numVariables == 1) {
		ofid <<"Node"<<TAB<<"u"<<TAB<<"q"<<endl;
	} else {
		ofid <<"Node"<<TAB<<"Variable"<<"u"<<TAB<<"q"<<endl;
	}
	for(nodeIdx = 0; nodeIdx < numNodes; nodeIdx++) {
		for(variableIdx = 0; variableIdx < numVariables; variableIdx++ ) {
			rowDofIdx = nodeIdx*numVariables + variableIdx;
			// Output solution.
			if(numVariables == 1) {
				ofid <<nodeIdx<<TAB<<u[ rowDofIdx ]<<TAB<<q[ rowDofIdx ]<<endl;
			} else {
				ofid <<nodeIdx<<TAB<<variableIdx<<TAB<<u[ rowDofIdx ]<<TAB<<q[ rowDofIdx ]<<endl;
			}
		}
	}
	ofid.close();

	return BEM_NO_ERROR;
}
