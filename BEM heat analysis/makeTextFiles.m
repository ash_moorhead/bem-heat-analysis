fid = fopen('BEM_Config.txt', 'w+');
% read in number of elements
clear all;
clear;

numEl = 200;
numNodes = 2* numEl;
totalLength = 740;
nodeSpacing = totalLength/numNodes;
x = 0;
y = 0;
nodes = zeros(numNodes, 2);

fidN = fopen('nodes.txt', 'w+');
fidE = fopen('elements.txt', 'w+');
fprintf(fidN, '%d\n', numNodes);
fprintf(fidE, '%d\t%d\n', numEl, 2);

%% Mesh boundary

% mesh  bottom
nodesToFit = floor(100/nodeSpacing);
for i = 1:nodesToFit + 1
    nodes(i, 1) = x;
    nodes(i, 2) = y;
    x = x + nodeSpacing;
    if (i == nodesToFit)
        x = 100;
        nodes(i, 1) = x;
        nodes(i, 2) = y;
        
        nodes(i-1, 1) = nodes(i-2, 1) +((x - nodes(i-2, 1))/2);
        break;
    end
end

%mesh right
start = i + 1;
nodesToFit = floor(150/nodeSpacing);
stop = start + nodesToFit;
for i = start:stop
    y = y + nodeSpacing;
    if (i == stop)
        y = 150;
        nodes(i, 1) = x;
        nodes(i, 2) = y;
        
        nodes(i-1, 2) = nodes(i-2, 2) +((y - nodes(i-2, 2))/2);
        break;
    else
        nodes(i, 1) = x;
        nodes(i, 2) = y;
    end
end

%mesh top
start = i + 1;
nodesToFit = floor(100/nodeSpacing);
stop = start + nodesToFit;
for i = start: stop
    x = x - nodeSpacing;
    if (i == stop)
        x = 0;
        nodes(i, 1) = x;
        nodes(i, 2) = y;
        
        nodes(i-1, 1) = nodes(i-2, 1) +((x - nodes(i-2, 1))/2);
        break;
    else
        nodes(i, 1) = x;
        nodes(i, 2) = y;
    end
end

%mesh left
start = i + 1;
nodesToFit = floor(150/nodeSpacing);
stop = start + nodesToFit;
for i = start:stop
    y = y - nodeSpacing;
    
    if (i == stop)
        y = 0;
        nodes(i-1, 2) = nodes(i-2, 2) +((y - nodes(i-2, 2))/2);
        break;
    else
        nodes(i, 1) = x;
        nodes(i, 2) = y;
    end
end

%% Mesh 100 degree resistor

x = 20;
y = 20;

% mesh  bottom
start = i;
nodesToFit = floor(30/nodeSpacing);
stop = start + nodesToFit;
for i = start:stop
    nodes(i, 1) = x;
    nodes(i, 2) = y;
    x = x + nodeSpacing;
    if (i == stop)
        x = 50;
        nodes(i, 1) = x;
        nodes(i, 2) = y;
        
        nodes(i-1, 1) = nodes(i-2, 1) +((x - nodes(i-2, 1))/2);
        break;
    end
end

%mesh right
start = i + 1;
nodesToFit = floor(10/nodeSpacing);
stop = start + nodesToFit - 1;
for i = start:stop
    y = y + nodeSpacing;
    nodes(i, 1) = x;
    nodes(i, 2) = y;
    if (i == stop)
        y = 30;
        nodes(i, 1) = x;
        nodes(i, 2) = y;
        
        nodes(i-1, 2) = nodes(i-2, 2) +((y - nodes(i-2, 2))/2);
        break;
    end
end

%mesh top
start = i + 1;
nodesToFit = floor(30/nodeSpacing);
stop = start + nodesToFit ;
for i = start: stop
    x = x - nodeSpacing;
    nodes(i, 1) = x;
    nodes(i, 2) = y;
    if (i == stop)
        x = 20;
        nodes(i, 1) = x;
        nodes(i, 2) = y;
        
        nodes(i-1, 1) = nodes(i-2, 1) +((x - nodes(i-2, 1))/2);
        break;
    end
end

%mesh left
start = i + 1;
nodesToFit = floor(10/nodeSpacing);
stop = start + nodesToFit - 1;
for i = start:stop
    y = y - nodeSpacing;
    
    if (i == stop)
        y = 20;
        nodes(i-1, 2) = nodes(i-2, 2) +((y - nodes(i-2, 2))/2);
        break;
    else
        nodes(i, 1) = x;
        nodes(i, 2) = y;
    end
end

%% Mesh 80 degree resistor

x = 20;
y = 60;

% mesh  bottom
start = i + 1;
nodesToFit = floor(10/nodeSpacing);
stop = start + nodesToFit - 1;
for i = start:stop
    nodes(i, 1) = x;
    nodes(i, 2) = y;
    x = x + nodeSpacing;
    if (i == stop)
        x = 30;
        nodes(i, 1) = x;
        nodes(i, 2) = y;
        
        nodes(i-1, 1) = nodes(i-2, 1) +((x - nodes(i-2, 1))/2);
        break;
    end
end

%mesh right
start = i + 1;
nodesToFit = floor(30/nodeSpacing);
stop = start + nodesToFit;
for i = start:stop
    y = y + nodeSpacing;
    nodes(i, 1) = x;
    nodes(i, 2) = y;
    if (i == stop)
        y = 90;
        nodes(i, 1) = x;
        nodes(i, 2) = y;
        
        nodes(i-1, 2) = nodes(i-2, 2) +((y - nodes(i-2, 2))/2);
        break;
    end
end

%mesh top
start = i + 1;
nodesToFit = floor(10/nodeSpacing);
stop = start + nodesToFit - 1;
for i = start: stop
    x = x - nodeSpacing;
    nodes(i, 1) = x;
    nodes(i, 2) = y;
    if (i == stop)
        x = 20;
        nodes(i, 1) = x;
        nodes(i, 2) = y;
        
        nodes(i-1, 1) = nodes(i-2, 1) +((x - nodes(i-2, 1))/2);
        break;
    end
end

%mesh left
start = i + 1;
nodesToFit = floor(30/nodeSpacing);
stop = start + nodesToFit;
for i = start:stop
    y = y - nodeSpacing;
    
    if (i == stop)
        y = 60;
        nodes(i-1, 2) = nodes(i-2, 2) +((y - nodes(i-2, 2))/2);
        break;
    else
        nodes(i, 1) = x;
        nodes(i, 2) = y;
    end
end

%% Mesh 40 degree

x = 60;
y = 110;

% mesh  bottom
start = i;
nodesToFit = floor(30/nodeSpacing);
stop = start + nodesToFit;
for i = start:stop
    nodes(i, 1) = x;
    nodes(i, 2) = y;
    x = x + nodeSpacing;
    if (i == stop)
        x = 90;
        nodes(i, 1) = x;
        nodes(i, 2) = y;
        
        nodes(i-1, 1) = nodes(i-2, 1) +((x - nodes(i-2, 1))/2);
        break;
    end
end

%mesh right
start = i + 1;
nodesToFit = floor(10/nodeSpacing);
stop = start + nodesToFit - 1;
for i = start:stop
    y = y + nodeSpacing;
    nodes(i, 1) = x;
    nodes(i, 2) = y;
    if (i == stop)
        y = 120;
        nodes(i, 1) = x;
        nodes(i, 2) = y;
        
        nodes(i-1, 2) = nodes(i-2, 2) +((y - nodes(i-2, 2))/2);
        break;
    end
end

%mesh top
start = i + 1;
nodesToFit = floor(30/nodeSpacing);
stop = start + nodesToFit;
for i = start: stop
    x = x - nodeSpacing;
    nodes(i, 1) = x;
    nodes(i, 2) = y;
    if (i == stop)
        x = 60;
        nodes(i, 1) = x;
        nodes(i, 2) = y;
        
        nodes(i-1, 1) = nodes(i-2, 1) +((x - nodes(i-2, 1))/2);
        break;
    end
end

%mesh left
start = i + 1;
nodesToFit = floor(10/nodeSpacing);
stop = start + nodesToFit - 1;
for i = start:stop
    y = y - nodeSpacing;
    
    if (i == stop)
        y = 110;
        nodes(i-1, 2) = nodes(i-2, 2) +((y - nodes(i-2, 2))/2);
        break;
    else
        nodes(i, 1) = x;
        nodes(i, 2) = y;
    end
end

%% write to file

dlmwrite('nodes.txt', nodes, '-append', 'delimiter', ' ','newline', 'pc');

%% make element file

elements = zeros(numEl, 2);

for i = 1:numEl
   
    elements(i, 1) = i - 1;
    elements(i, 2) = i;
    
end

dlmwrite('elements.txt', elements, '-append', 'delimiter', ' ','newline', 'pc');


















fclose('all');
