/*
	This file contains analytic functions for BEM problems.
*/

#include "BEM_AnalyticFunctions.h"
#include "BEM_Constants.h"
#include "Constants.h"
#include "Misc.h"

// Return the analytic function value for the given analysis, function and value type.
int BEMAnalyticFunction(
	const int analysisType,									// The analysis type for the analytic function. See BEM_Constants.h for valid analysis types.
	const int analyticFunction,								// The analytic function. See BEM_AnalyticFunctions.h for valid analytic function types.
	const int analyticValueType,							// The type of analytic value (u or q) to return. See BEM_AnalyticFunctions.h for valid analytic value types.
	double position[ MAX_NUM_DIMENSIONS ],					// position[ coordinateIdx ]. The coordinateIdx'th coordinate value to evaluate the analytic function at.
	double* analyticValue									// On exit, the value of the analytic function.
	)
{
	*analyticValue = 0.0;

	switch(analysisType) {
	case LAPLACE_ANALYSIS_TYPE:
		// Laplace's equation.
		switch(analyticFunction) {
		case LAPLACE_2D_ANALTYIC_FUNCTION_1:
			// On a circle. u(x,y) = x^2 + 2xy -y^2; q(x,y) = (2x^2 + 4xy - 2y^2)/R
			switch(analyticValueType) {
			case U_ANALYTIC_VALUE_TYPE:
				*analyticValue = position[ 0 ]*position[ 0 ] + 2.0*position[ 0 ]*position[ 1 ] - position[ 1 ]*position[ 1 ];
				break;
			case Q_ANALYTIC_VALUE_TYPE:
				*analyticValue = (2.0*position[ 0 ]*position[ 0 ] + 4.0*position[ 0 ]*position[ 1 ] - 2.0*position[ 1 ]*position[ 1 ])/
					(sqrt(position[ 0 ]*position[ 0 ] + position[ 1 ]*position[ 1 ]));
				break;
			default:
				return ANALYTIC_FUNCTIONS_INVALID_VALUE_TYPE_ERROR;
				break;
			}
			break;
		default:
			return ANALYTIC_FUNCTIONS_INVALID_FUNCTION_ERROR;
			break;
		}
		break;
	case LINEAR_ELASTICITY_ANALYSIS_TYPE:
		// Linear elasticity
		return ANALYTIC_FUNCTIONS_NOT_IMPLEMENTED_ERROR;
		break;
	default:
		return ANALYTIC_FUNCTIONS_INVALID_ANALYSIS_TYPE;
		break;
	}

	return ANALYTIC_FUNCTIONS_NO_ERROR;
}
