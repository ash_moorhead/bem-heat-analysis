/*

  Define routines to compute Gauss point locations and weights for various Gaussian quadrature schemes
  and numbers of Gauss points.

*/

#include "GaussianQuadrature.h"

// This routine creates the weights and abscissae for a logarithmic Gaussian quadrature scheme i.e., \int_0^1 f(x)ln(x)dx
int GaussianQuadratureLogarithmicPointsCreate(
	const int N,			// The required number of requested logarithmic points.
	double **xi,			// On exit, xi[i] the i'th logarithmic Gaussian quadrature location.
	double **wgt			// On exit, wgt[i] the i'th logarithmic Gaussian quadrature weight.
	)	
{
	//Check inputs
	if(N <= 1 || N > 10) {
		return GAUSSIAN_QUADRATURE_INVALID_NUM_GAUSS_POINTS_ERROR;
	}
	if(*xi != NULL) {
		return GAUSSIAN_QUADRATURE_XI_NOT_NULL_ERROR;
	}
	if(*wgt != NULL) {
		return GAUSSIAN_QUADRATURE_WGT_NOT_NULL_ERROR;
	}

	//Allocate memory
	*xi = new double [ N ];
	*wgt = new double [ N ];

	//Set up Gauss point positions and weights
	switch (N) {
		case 2:
			(*xi)[0]=0.11200880;
			(*xi)[1]=0.60227691;
			(*wgt)[0]=-0.71853931;
			(*wgt)[1]=-0.28146068;
			break;
		case 3:
			(*xi)[0]=0.063890792;
			(*xi)[1]=0.36899706;
			(*xi)[2]=0.76688030;
			(*wgt)[0]=-0.51340455;
			(*wgt)[1]=-0.39198004;
			(*wgt)[2]=-0.094615406; 
			break;
		case 4:
			(*xi)[0]=0.041448480; 
			(*xi)[1]=0.24527491;
			(*xi)[2]=0.55616545;
			(*xi)[3]=0.84898239;
			(*wgt)[0]=-0.38346406;
			(*wgt)[1]=-0.38687532;
			(*wgt)[2]=-0.19043513;
			(*wgt)[3]=-0.039225487;
			break;
		case 5:
			(*xi)[0]=0.029134472; 
			(*xi)[1]=0.17397721;
			(*xi)[2]=0.41170251;
			(*xi)[3]=0.67731417;
			(*xi)[4]=0.89477136;
			(*wgt)[0]=-0.29789346;
			(*wgt)[1]=-0.34977622;
			(*wgt)[2]=-0.23448829;
			(*wgt)[3]=-0.09893046;
			(*wgt)[4]=-0.018911552;
			break;
		case 6:
			(*xi)[0]=0.021634005;
			(*xi)[1]=0.12958339;
			(*xi)[2]=0.31402045;
			(*xi)[3]=0.53865721;
			(*xi)[4]=0.75691533;
			(*xi)[5]=0.92266884;
			(*wgt)[0]=-0.23876366;
			(*wgt)[1]=-0.30828657;
			(*wgt)[2]=-0.24531742;
			(*wgt)[3]=-0.14200875;
			(*wgt)[4]=-0.055454622;
			(*wgt)[5]=-0.010168958;
			break;
		case 7:
			(*xi)[0]=0.016719355;
			(*xi)[1]=0.10018568;
			(*xi)[2]=0.24629424;
			(*xi)[3]=0.43346349;
			(*xi)[4]=0.63235098;
			(*xi)[5]=0.81111862;
			(*xi)[6]=0.94084816;
			(*wgt)[0]=-0.19616938;
			(*wgt)[1]=-0.27030264;
			(*wgt)[2]=-0.23968187;
			(*wgt)[3]=-0.16577577;
			(*wgt)[4]=-0.088943226;
			(*wgt)[5]=-0.033194304;
			(*wgt)[6]=-0.0059327869; 
			break;
		case 8:
			(*xi)[0]=0.013320243;
			(*xi)[1]=0.079750427; 
			(*xi)[2]=0.19787102;
			(*xi)[3]=0.35415398;
			(*xi)[4]=0.52945857;
			(*xi)[5]=0.70181452;
			(*xi)[6]=0.84937932;
			(*xi)[7]=0.95332645;
			(*wgt)[0]=-0.16441660;
			(*wgt)[1]=-0.23752560;
			(*wgt)[2]=-0.22684198;
			(*wgt)[3]=-0.17575408;
			(*wgt)[4]=-0.11292402;
			(*wgt)[5]=-0.057872212;
			(*wgt)[6]=-0.020979074;
			(*wgt)[7]=-0.0036864071;
			break;
		case 9:
			(*xi)[0]=0.010869338; 
			(*xi)[1]=0.064983682; 
			(*xi)[2]=0.16222943;
			(*xi)[3]=0.29374996;
			(*xi)[4]=0.44663195;
			(*xi)[5]=0.60548172;
			(*xi)[6]=0.75411017;
			(*xi)[7]=0.87726585;
			(*xi)[8]=0.96225056;
			(*wgt)[0]=-0.14006846;
			(*wgt)[1]=-0.20977224;
			(*wgt)[2]=-0.21142716;
			(*wgt)[3]=-0.17715622;
			(*wgt)[4]=-0.12779920;
			(*wgt)[5]=-0.078478879;
			(*wgt)[6]=-0.039022490;
			(*wgt)[7]=-0.013867290;
			(*wgt)[8]=-0.0024080402;
			break;
		case 10:
			(*xi)[0]=0.0090425944; 
			(*xi)[1]=0.053971054;
			(*xi)[2]=0.13531134;
			(*xi)[3]=0.24705169;
			(*xi)[4]=0.38021171;
			(*xi)[5]=0.52379159;
			(*xi)[6]=0.66577472;
			(*xi)[7]=0.79419019;
			(*xi)[8]=0.89816102;
			(*xi)[9]=0.96884798;
			(*wgt)[0]=-0.12095474;
			(*wgt)[1]=-0.18636310;
			(*wgt)[2]=-0.19566066;
			(*wgt)[3]=-0.17357723;
			(*wgt)[4]=-0.13569597;
			(*wgt)[5]=-0.093647084;
			(*wgt)[6]=-0.055787938;
			(*wgt)[7]=-0.027159893;
			(*wgt)[8]=-0.0095151992;
			(*wgt)[9]=-0.0016381586;
			break;
	}

	return GAUSSIAN_QUADRATURE_NO_ERROR;
}

// This routine creates the weights and abscissae for a Gaussian Legendre quadrature scheme i.e., \int_alpha^beta f(x)dx
// This routine is adapted from Numerical Recipes.
int GaussianQuadratureLegendrePointsCreate(
	const int N, 			// The required number of requested Legendre points.
	const double alpha,		// The integral start
	const double beta,		// The integral finish
	double **xi,			// On exit, xi[i] the i'th Legendre Gaussian quadrature location.
	double **wgt			// On exit, wgt[i] the i'th Legendre Gaussian quadrature weight.
	)
{
	int i, j, m;
	double p1, p2, p3, pp, xl, xm, z, z1;

	//Check inputs
	if(N <= 1 || N > GAUSSIAN_LEGENDRE_QUADRATURE_MAX_NUM_GAUSS_POINTS ) {
		return GAUSSIAN_QUADRATURE_INVALID_NUM_GAUSS_POINTS_ERROR;
	}
	if(*xi != NULL) {
		return GAUSSIAN_QUADRATURE_XI_NOT_NULL_ERROR;
	}
	if(*wgt != NULL) {
		return GAUSSIAN_QUADRATURE_WGT_NOT_NULL_ERROR;
	}

	//Allocate memory
	*xi = new double [ N ];
	*wgt = new double [ N ];

	//Calculate the Gauss points and weights from the roots of Legendre polynomials 
    m = (N + 1)/2;
    xm = 0.5*(beta + alpha);
    xl = 0.5*(beta - alpha);
    z1 = 999999999.0;
    for(i = 1; i <= m; i++) {
      z = cos(acos(-1.0)*(i - 0.25)/(N + 0.5));
      while (abs(z - z1) > 0.00000000000001) {
        p1 = 1.0;
        p2 = 0.0;
        for(j = 1; j<=N; j++) {
          p3 = p2;
          p2 = p1;
          p1 = ((2.0*j - 1.0)*z*p2 - (j - 1.0)*p3)/j;
		}
        pp = N*(z*p1 - p2)/(z*z - 1.0);
        z1 = z;
        z = z1 - p1/pp;
	  }
      (*xi)[i-1] = xm-xl*z;
      (*xi)[N-i] = xm+xl*z;
      (*wgt)[i-1] = 2.0*xl/((1.0-z*z)*pp*pp);
      (*wgt)[N-i] = (*wgt)[i-1];
	}

	return GAUSSIAN_QUADRATURE_NO_ERROR;
}

// This routine destroys the Gaussian quadrature location and weight arrays.
int GaussianQuadraturePointsDestroy(
	double **xi,			// The Gaussian quadrature xi locations to destroy.
	double **wgt			// The Gaussian quadrature weights to destroy.	
	)
{
	if(*xi != NULL) delete [] xi;
	if(*wgt != NULL) delete [] wgt;

	return GAUSSIAN_QUADRATURE_NO_ERROR;
}
 