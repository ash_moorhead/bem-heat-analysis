/*

Welcome to Part 3 of your FEM/BEM project for ENGSCI753 2015.

*/

#include "BEM_Main.h"
#include "BEM_Constants.h"
#include "BEM_SetupAndFinalise.h"
#include "BEM_Routines.h"
#include "BEM_AnalysisAndOutput.h"
#include "BEM_AnalyticFunctions.h"
#include "CommandLineFilter.h"
#include "Constants.h"
#include "GaussianQuadrature.h"
#include "MatrixOperations.h"
#include <stdlib.h>
#include <iostream>
#include <math.h>

void main(int argc, char **argv)
{
	// Variable declarations

	int 	analysisType,				        // The analysis type (Laplace, elasticity etc.). See BEM_Constants.h
			numDimensions,						// Number of dimensions in the problem.
			meshType,							// The type of mesh (read in, generated etc.). See BEM_Constants.h
			elementType,						// The elements to be used (linear, quadratic etc.). See BEM_BasisFunctions.h
			analyticTest,						// The type of analytic test (no, u, q). See BEM_Constants.h
			err = BEM_NO_ERROR;					// The error code.

	int     numNodes = 0,						// The total number of nodes in the mesh.
			numElements = 0,					// The total number of elements in the mesh.
			nodesPerElement = 0,				// The number of nodes per element.
			numVariables = 0,					// The number of variables per node.
			numDOFS = 0;						// The number of degrees-of-freedom in the problem.
	
	int		columnDofIdx,						// Column degree-of-freedom (DOF) index
			coordinateIdx,						// Coordinate dimension index
			elementIdx,							// Element index.
			gaussPointIdx,						// Gauss point index.	
			integrationSchemeIdx,				// Integration scheme index.
			localNodeIdx,						// Local element node index.
			localNode,							// Local node number.
			nodeIdx,							// Node index.
			rowDofIdx,							// Row degree-of-freedom (DOF) index
			variableIdx;						// Variable index.

	double  absoluteError,						// Absolute error.
			averageElementSize,					// Average element size.
			analyticAverageElementSize,			// The analytic average element size (based on a circle).
			analyticDomainSolution,				// The analytic domain solution.
			analyticIntegral,					// The analytic value of the integral.
			circleRadius,						// A circle mesh radius.
			domainPoint[ MAX_NUM_DIMENSIONS ],	// domainPoint[ coordinateIdx ]. The coordinateIdx'th coordinate location of a domain point.
			domainSolution,						// The domain solution
			elementLength,						// The length of an element	.
			integral,							// The value of the integral
			Jacobian,							// The Jacobian at a Gauss point.
			location[ MAX_NUM_DIMENSIONS ],		// location[ coordinateIdx ]. The coordinateIdx'th coordinate location of a point.
			normal[ MAX_NUM_DIMENSIONS ],		// normal[ coordinateIdx ]. The coordinateIdx'th coordinate of the normal at a point.
			percentageError,					// Percentage error.
			pLocation[ MAX_NUM_DIMENSIONS ],	// The location of the singular point
			tangent[ MAX_NUM_DIMENSIONS ],		// tangent[ coordinateIdx ]. The coordinateIdx'th coordinate of the tangent at a point.	
			tangentLength,						// The length of a tangent vector.
			totalLength,						// The total length of elements in a mesh.	
			wgt,								// The Gauss point weight.
			xi;									// The Gauss point xi location.

	int		*nodeCorners = NULL,				// nodeCorners[ nodeIdx ] to indicate the type of corner at the nodeIdx'th node. See BEM_Constants.h
			**nodeBoundaryConditions = NULL,	// nodeBoundaryConditions[ nodeIdx ][ variableIdx ]. The type of boundary condition (no, essential, natural etc.) for the variableIdx'th variable at the nodeIdx'th node. See BEM_Constants.h
			*numGaussPoints = NULL,				// numGaussPoints[ integrationSchemeIdx ]. The number of Gauss points for the integrationSchemeIdx'th integration scheme.
			**elementNodes = NULL;				// elementNodes[ elementIdx ][ localNodeIdx ]. The node number for the localNodeIdx'th local node in the elementIdx'th element.

	double	**geometry = NULL,					// geometry[ nodeIdx ][ coordinateIdx ]. The coordinateIdx'th coordinate location (x, y etc.) for the nodeIdx'th node.
			**elementDOFS = NULL,				// elementDOFS[ elementIdx ][ localNodeIdx ]. Temporary array to hold the element degree-of-freedom for the localNodeIdx'th local node in the elementIdx'th element.
			*x = NULL,							// x[ rowDofIdx ]. The linear system solution vector
			*f = NULL,							// f[ rowDofIdx ]. The RHS load vector
			*u = NULL,							// u[ rowDofIdx ]. The u result vector
			*q = NULL,							// q[ rowDofIdx ]. The q result vector
			*uAnalytic = NULL,					// uAnalytic[ rowDofIdx ]. The analytic u results vector
			*qAnalytic = NULL,					// qAnalytic[ rowDofIdx ]. The analytic q results vector
			**gaussLocations = NULL,			// gaussLocations[ integrationSchemeIdx ][ gaussPointIdx ]. The xi location of the gaussPointIdx'th Gauss point for the integrationSchemeIdx'th integration scheme.
			**gaussWeights = NULL;				// gaussWeights[ integrationSchemeIdx ][ gaussPointIdx ]. The weight for the gaussPointIdx'th Gauss point for the integrationSchemeIdx'th integration scheme.

	double	**A = NULL,							// A[ rowDofIdx ][ columnDofIdx ]. Global BEM A matrix
			**B = NULL,							// B[ rowDofIdx ][ columnDofIdx ]. Global BEM B matrix
			**C = NULL,	
			**B_mat = NULL,						// C[ rowDofIdx ][ columnDofIdx ]. Linear Solver matrix
			**origC= NULL;					    // origC[ rowDofIdx ][ columnDofIdx ]. Copy of the C matrix for output

	ofstream ofid;								// Output stream ID.

	// -----------------------------------------------------------------------------------------------------------
	//
	//	Part 1: Read in config and setup problem arrays
	//
	// -----------------------------------------------------------------------------------------------------------

	//	Read in config and setup problem variables
	CommandLineFilter *cmd = new CommandLineFilter;
	cmd->ReadFromFile("BEM_config.txt");

	// Setup problem
	err = BEMSetupProblem(cmd, &numDimensions);
	if(err != BEM_NO_ERROR) {
		cout << "ERROR: Error code "<<err<<" when setting up BEM problem."<<endl;
		PAUSE
	}
	// Setup geometry
	err = BEMSetupGeometry(cmd, numDimensions, &meshType, &elementType, &numNodes, &numElements, 
		&nodesPerElement, &elementNodes, &nodeCorners, &geometry, &averageElementSize);
	if(err != BEM_NO_ERROR) {
		cout << "ERROR: Error code "<<err<<" when setting up BEM geometry."<<endl;
		PAUSE
	}
	// Setup analysis (problem equation)
	err = BEMSetupAnalysis(cmd,	numDimensions, meshType, numNodes, geometry, &analysisType,	
		&numVariables, &numDOFS, &analyticTest, &u, &q, &uAnalytic, &qAnalytic);
	if(err != BEM_NO_ERROR) {
		cout << "ERROR: Error code "<<err<<" when setting up BEM analysis."<<endl;
		PAUSE
	}
	// Setup boundary conditions
	err =  BEMSetupBoundaryConditions(cmd, numDimensions, meshType, numNodes, analysisType,
		numVariables, numDOFS, analyticTest, u, q, uAnalytic, qAnalytic, &nodeBoundaryConditions);
	if(err != BEM_NO_ERROR) {
		cout << "ERROR: Error code "<<err<<" when setting up BEM boundary conditions."<<endl;
		PAUSE
	}
	// Setup numerical arrays and vectors
	err = BEMSetupArrays(numDimensions, elementType, numDOFS, &elementDOFS, &A, &B, &C, &origC, &x, &f);
	if(err != BEM_NO_ERROR) {
		cout << "ERROR: Error code "<<err<<" when setting up BEM arrays."<<endl;
		PAUSE
	}

	B_mat = new double* [numDOFS];
	for (int i = 0; i < numDOFS; i++) {
		B_mat[i] = new double[numDOFS];
		for (int j = 0; j < numDOFS; j++) {
			B_mat[i][i] = 0.0;
		}
	}


	// -----------------------------------------------------------------------------------------------------------
	//
	//	Part 2: Setup and test integration schemes
	//
	// -----------------------------------------------------------------------------------------------------------

	// Allocate the Gaussian quadrature integration schemes
	err = IntegrationSchemesSetup(&numGaussPoints, &gaussLocations, &gaussWeights);
	if(err != BEM_NO_ERROR) {
		cout << "ERROR: Error code "<<err<<" when setting up integration schemes."<<endl;
		PAUSE
	}
	// Test integration by calculating average element length for each of the Gaussian Legendre integration schemes.
	cout << "Integration test 1: "<<endl;
	cout << "=================== "<<endl;
	cout << "Element type = "<<elementType<<endl;
	cout << "Number of elements = "<<numElements<<endl;
	if(meshType == CIRCLE_GENERATED_MESH) {
		circleRadius = 0.0;
		for(coordinateIdx = 0; coordinateIdx < numDimensions; coordinateIdx++) {
			circleRadius += geometry[ 0 ][ coordinateIdx ]*geometry[ 0 ][ coordinateIdx ];
		}
		circleRadius = sqrt(circleRadius);
		analyticAverageElementSize = 2.0*PI*circleRadius/(double)numElements;
		cout << "Analytic average element size = "<<analyticAverageElementSize<<endl;
	}
	for(integrationSchemeIdx = BEM_LOW_GAUSS_INTEGRATION_SCHEME; integrationSchemeIdx <= BEM_HIGH_GAUSS_INTEGRATION_SCHEME; integrationSchemeIdx++) {
		cout << "Integration scheme #"<<integrationSchemeIdx<<endl;
		cout << "   Number of Gauss points = "<<numGaussPoints[ integrationSchemeIdx ]<<endl;
		totalLength = 0.0;
		for(elementIdx = 0; elementIdx < numElements; elementIdx++) {
			// Setup local elemenet DOFS
			for(coordinateIdx = 0; coordinateIdx < numDimensions; coordinateIdx++) {
				//Get the element geometric information
				for(localNodeIdx = 0; localNodeIdx < elementType + 1; localNodeIdx++) {
					localNode = elementNodes[ elementIdx ][ localNodeIdx ];
					elementDOFS[ coordinateIdx ][ localNodeIdx ] = geometry[ localNode ][ coordinateIdx ];
				}
			}
			elementLength = 0.0;
			for(gaussPointIdx = 0; gaussPointIdx < numGaussPoints[ integrationSchemeIdx ]; gaussPointIdx++) {
				xi = gaussLocations[ integrationSchemeIdx ][ gaussPointIdx ];
				wgt = gaussWeights[ integrationSchemeIdx ][ gaussPointIdx ];
				err = CalculateGeometricInformation(numDimensions, elementType, xi, elementDOFS, location, tangent, normal, &Jacobian);
				if(err != BEM_NO_ERROR) {
					cout << "ERROR: Error code "<<err<<" when calculating geometric information."<<endl;
					PAUSE
				}
				tangentLength = 0.0;
				for(coordinateIdx = 0; coordinateIdx < numDimensions; coordinateIdx++) {
					tangentLength += tangent[ coordinateIdx]*tangent[ coordinateIdx ];
				}
				tangentLength = sqrt(tangentLength);
				elementLength = elementLength + tangentLength*wgt;
			}
			totalLength = totalLength + elementLength;
		}
		averageElementSize = totalLength/(double)numElements;
		if(meshType == CIRCLE_GENERATED_MESH) {
			absoluteError = averageElementSize - analyticAverageElementSize;
			percentageError = 100.0*absoluteError/analyticAverageElementSize;
			cout << "   Average element size = "<<averageElementSize<<"; percentage error = "<<percentageError<<"%"<<endl;
		} else {
			cout << "   Average element size = "<<averageElementSize<<endl;
		}
	}
	cout << endl;
	// Perform a 2nd integration test. Evaluate the integral int_0^1.x^2.ln(x).dx using the four integration schemes. 
	// The analytic value for this integral is -1/9. Compare the errors between the integration schemes.
	cout << "Integration test 2: "<<endl;
	cout << "=================== "<<endl;
	analyticIntegral = -1.0/9.0;
	cout << "Integral : int_0^1.x^2.ln(x).dx"<<endl;
	cout << "Analytic integral = "<<analyticIntegral<<endl;
	for(integrationSchemeIdx = BEM_LOG_GAUSS_INTEGRATION_SCHEME; integrationSchemeIdx <= BEM_HIGH_GAUSS_INTEGRATION_SCHEME; integrationSchemeIdx++) {
		cout << "Integration scheme #"<<integrationSchemeIdx<<endl;
		cout << "   Number of Gauss points = "<<numGaussPoints[ integrationSchemeIdx ]<<endl;

		integral = 0.0;
		for (gaussPointIdx = 0; gaussPointIdx < numGaussPoints[integrationSchemeIdx]; gaussPointIdx++) {
			
			xi = gaussLocations[integrationSchemeIdx][gaussPointIdx];
			wgt = gaussWeights[integrationSchemeIdx][gaussPointIdx];

			if (integrationSchemeIdx == BEM_LOG_GAUSS_INTEGRATION_SCHEME){
				integral += (xi*xi)*wgt;			
			}
			else{
				integral += (xi*xi * log(xi))*wgt;
			}
			

		}

		absoluteError = integral - analyticIntegral;
		percentageError = 100.0*absoluteError/analyticIntegral;
		cout << "   Integral = "<<integral<<"; percentage error = "<<percentageError<<"%"<<endl;
	}
	cout << endl;
	// Perform a 3rd integration test. Evaluate the integral int_0^1.sqrt(x)ln(x).dx using the four integration schemes. 
	// The analytic value for this integral is -4/9. Compare the errors between the integration schemes.
	cout << "Integration test 3: "<<endl;
	cout << "=================== "<<endl;
	analyticIntegral = -4.0/9.0;
	cout << "Integral : int_0^1.sqrt(x)ln(x).dx"<<endl;
	cout << "Analytic integral = "<<analyticIntegral<<endl;
	for(integrationSchemeIdx = BEM_LOG_GAUSS_INTEGRATION_SCHEME; integrationSchemeIdx <= BEM_HIGH_GAUSS_INTEGRATION_SCHEME; integrationSchemeIdx++) {
		cout << "Integration scheme #"<<integrationSchemeIdx<<endl;
		cout << "   Number of Gauss points = "<<numGaussPoints[ integrationSchemeIdx ]<<endl;

		integral = 0.0;
		for (gaussPointIdx = 0; gaussPointIdx < numGaussPoints[integrationSchemeIdx]; gaussPointIdx++) {
			xi = gaussLocations[integrationSchemeIdx][gaussPointIdx];
			wgt = gaussWeights[integrationSchemeIdx][gaussPointIdx];

			if (integrationSchemeIdx == BEM_LOG_GAUSS_INTEGRATION_SCHEME){
				integral += (sqrt(xi))*wgt;
			}
			else{
				integral += (sqrt(xi) * log(xi))*wgt;
			}

		}

		absoluteError = integral - analyticIntegral;
		percentageError = 100.0*absoluteError/analyticIntegral;
		cout << "   Integral = "<<integral<<"; percentage error = "<<percentageError<<"%"<<endl;
	}
	cout << endl;
	cout << "Enter a character to continue......" << endl;
	PAUSE

	// -----------------------------------------------------------------------------------------------------------
	//
	//	Part 3: Calculate the BEM and solution matrices
	//
	// -----------------------------------------------------------------------------------------------------------

	// Loop over the nodes (singular points)
	for(nodeIdx = 0; nodeIdx < numNodes; nodeIdx++) {
		// Set the location of the singular point to the location of the current node
		for(coordinateIdx = 0; coordinateIdx < numDimensions; coordinateIdx++) {
			pLocation[ coordinateIdx ] = geometry[ nodeIdx ][ coordinateIdx ];
		}
		// Loop over the variables
		for(variableIdx = 0; variableIdx < numVariables; variableIdx++) {
			// Calculate the row we are computing
			rowDofIdx = nodeIdx*numVariables + variableIdx;

			//******************************************************
			//*** ADD CODE HERE TO COMPUTE ROW OF A & B MATRICES ***
			//******************************************************

			// call CalculateMatrixEntries j times (for each node) and it assembles the global A and B matrices

			err = CalculateMatrixEntries(numDimensions, elementType, numElements, averageElementSize, analysisType, numVariables, numDOFS, elementNodes, pLocation, geometry, elementDOFS, numGaussPoints, gaussLocations, gaussWeights, 1, rowDofIdx, A, B);
			if (err != BEM_NO_ERROR) {
				cout << "ERROR: Error code " << err << " when setting up BEM matrices." << endl;
				PAUSE
			}
		}
	}	

	//**************************************************
	//*** APPLY BOUNDARY CONDITIONS TO GET C AND F ***
	//**************************************************
	
	err = BoundaryConditions(A, B, nodeBoundaryConditions, f, u, q, numDOFS, B_mat, C);
	if (err != BEM_NO_ERROR){
		cout << "ERROR: Error code " << err << " missing boundary conditions." << endl;
		PAUSE
	}

	// Store a copy of the linear system for output:
	for(rowDofIdx = 0; rowDofIdx < numDOFS; rowDofIdx++) {
		for(columnDofIdx = 0; columnDofIdx < numDOFS; columnDofIdx++) {
			origC[ rowDofIdx ][ columnDofIdx ] = C[ rowDofIdx ][ columnDofIdx ];
		}
		// Copying the f array to the solution (the array passed to the SolveAxB routine gets overwritten with the solved 'solution' values
		x[ rowDofIdx ] = f[ rowDofIdx ];
	}

	// -----------------------------------------------------------------------------------------------------------
	//
	//	Part 4: Solving the system
	//
	// -----------------------------------------------------------------------------------------------------------

	// RHS values passed in through solution, and overwritten with new values 
	SolveAxB(C, x, numDOFS);

	// -----------------------------------------------------------------------------------------------------------
	//
	//	Part 5: Post solution processing
	//
	// -----------------------------------------------------------------------------------------------------------

	//Copy solution from solution to u or q

	//**************************************
	//*** ADD CODE HERE TO COPY SOLUTION ***
	//**************************************

	err = Rearranging(nodeBoundaryConditions, u, q, x, numDOFS);
	if (err != BEM_NO_ERROR){
		cout << "ERROR: Error code " << err << " missing boundary conditions." << endl;
		PAUSE
	}

	// -----------------------------------------------------------------------------------------------------------
	//
	//	Part 6: Output and analysis
	//
	// -----------------------------------------------------------------------------------------------------------

	//If we are doing an analytic test the write results
	if(analyticTest) {
		if(meshType == CIRCLE_GENERATED_MESH) {
			averageElementSize = analyticAverageElementSize;
		}
		BEMAnalyticAnalysis("BEM_AnalyticResults.xls", elementType, numNodes, numElements, numVariables,
			numDOFS, averageElementSize, u, q, uAnalytic, qAnalytic);
		if(err != BEM_NO_ERROR) {
			cout << "ERROR: Error code "<<err<<" when outputing BEM analytic results."<<endl;
			PAUSE
		}
	}
	// Printing the data to a file for checking and analysis:
	err = BEMLinearSystemsOutput("BEM_LinearSystem.xls", numDOFS, u, q, A, B, origC, x, f);
	if(err != BEM_NO_ERROR) {
		cout << "ERROR: Error code "<<err<<" when outputing BEM linear system."<<endl;
		PAUSE
	}
	// Write out the results
	err = BEMResultsOutput("BEM_Results.xls", numNodes, numVariables, u, q);
	if(err != BEM_NO_ERROR) {
		cout << "ERROR: Error code "<<err<<" when outputing BEM results."<<endl;
		PAUSE
	}

	// -----------------------------------------------------------------------------------------------------------
	//
	//	Part 7: Domain solutions
	//
	// -----------------------------------------------------------------------------------------------------------

	// Set the domain point location
	domainPoint[ 0 ] = 67.5;
	domainPoint[ 1 ] = 47.5;
	// Compute the domain solution
	domainSolution = 0.0;

	//************************************************
	//*** ADD CODE HERE TO COMPUTE DOMAIN SOLUTION ***
	//************************************************


	double Au = 0, 
		Bq = 0; 

	err = CalculateMatrixEntries(numDimensions, elementType, numElements, averageElementSize, analysisType, numVariables, numDOFS, elementNodes, domainPoint, geometry, elementDOFS, numGaussPoints, gaussLocations, gaussWeights, 0, 0, A, B);
	if (err != BEM_NO_ERROR) { 
		cout << "ERROR: Error code " << err << " when calculating matrix entries" << endl; 
		PAUSE 
	} 
	for (int i = 0; i < numDOFS; i++){
		Au += A[0][i] * u[i]; 
		Bq += B[0][i] * q[i]; 
	} 
	domainSolution = Bq - Au;


	// Write out the solution
	cout << endl;
	cout << "Domain solution: "<<endl;
	cout << "================ "<<endl;
	cout << "Domain location = [ "<<domainPoint[ 0 ];
	for(coordinateIdx = 1; coordinateIdx < numDimensions; coordinateIdx++) {
		cout << ", "<<domainPoint[ coordinateIdx ];
	}
	cout << " ]"<<endl;
	if(analyticTest) {
		if(meshType == CIRCLE_GENERATED_MESH) {
			err = BEMAnalyticFunction(analysisType, LAPLACE_2D_ANALTYIC_FUNCTION_1, U_ANALYTIC_VALUE_TYPE, 
				domainPoint, &analyticDomainSolution);
			if(err != BEM_NO_ERROR) {
				cout << "ERROR: Error code "<<err<<" when calculating analytic function."<<endl;
				PAUSE
			}
			absoluteError = domainSolution - analyticDomainSolution;
			percentageError = 100.0*absoluteError/analyticDomainSolution;
			cout << "Analytic domain solution = "<<analyticDomainSolution<<endl;
			cout << "Domain solution = "<<domainSolution<<"; percentage error = "<<percentageError<<"%"<<endl;
		} else {
			cout << "Domain solution = "<<domainSolution<<endl;
		}
	} else {
		cout << "Domain solution = "<<domainSolution<<endl;
	}
	cout << endl;
	cout << "Enter a character to continue......"<<endl;
	PAUSE

	// -----------------------------------------------------------------------------------------------------------
	//
	//	Part 8: Finalisation and cleanup
	//
	// -----------------------------------------------------------------------------------------------------------

	// Finalise the integration arrays
	err = IntegrationSchemesFinalise(&numGaussPoints, &gaussLocations, &gaussWeights);
	if(err != BEM_NO_ERROR) {
		cout << "ERROR: Error code "<<err<<" when finalising up integration schemes."<<endl;
		PAUSE
	}
	// Finalise numerical arrays and vectors
	err = BEMFinaliseArrays(numDimensions, numDOFS, &elementDOFS, &A, &B, &C, &origC, &x, &f);
	if(err != BEM_NO_ERROR) {
		cout << "ERROR: Error code "<<err<<" when finalising BEM arrays."<<endl;
		PAUSE
	}
	// Finalise boundary conditions
	err = BEMFinaliseBoundaryConditions(numNodes, &nodeBoundaryConditions);
	if(err != BEM_NO_ERROR) {
		cout << "ERROR: Error code "<<err<<" when finalising BEM boundary conditions."<<endl;
		PAUSE
	}
	// Finalise analysis
	err = BEMFinaliseAnalysis(&u, &q, &uAnalytic, &qAnalytic);
	if(err != BEM_NO_ERROR) {
		cout << "ERROR: Error code "<<err<<" when finalising BEM analysis."<<endl;
		PAUSE
	}
	// Finalise geometry
	err = BEMFinaliseGeometry(numNodes, numElements, &elementNodes, &nodeCorners, &geometry);
	if(err != BEM_NO_ERROR) {
		cout << "ERROR: Error code "<<err<<" when finalising BEM geometry."<<endl;
		PAUSE
	}
	// Finalise problem
	err = BEMFinaliseProblem();
	if(err != BEM_NO_ERROR) {
		cout << "ERROR: Error code "<<err<<" when finalising BEM problem."<<endl;
		PAUSE
	}

	// THE END

	cout << "Finished!"<<endl;

}