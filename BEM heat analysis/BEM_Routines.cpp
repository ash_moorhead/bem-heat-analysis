/*
  Define routines for the boundary element method
  */

#include "BEM_Routines.h"
#include "BEM_Constants.h"
#include "BEM_BasisFunctions.h"
#include "Constants.h"
#include "GaussianQuadrature.h"
#include "Misc.h"

// Calculates the value of the freespace Green's function
int GreensFunction(
	const int greensFunctionType,	// The type of Green's function to evaluate
	const double r,					// The distance to evaluate the Green's function at
	double* greensFunction			// On return, the value of the evaluated Green's function
	);

// Calculates the value of the derivative of the freespace Green's function
int DerivativeGreensFunction(
	const int greensFunctionType,		// The type of Green's function to evaluate
	const double r,						// The distance to evaluate the Green's function at
	const double drdn,					// The value of dr/dn to evaluate the Green's function with
	double* derivativeGreensFunction	// On return, the value of the evaluated derivative of the Green's function
	);

// Calculates the geometric information (location, tangent, normal, Jacobian etc.) at a xi location in an element.
int CalculateGeometricInformation(
	const int numDimensions,				// The number of dimensions
	const int elementType,					// The type of element
	const double xi,						// The xi location to calculate the geometric information at
	double** elementDofs,					// The element degrees-of-freedom for this element
	double location[MAX_NUM_DIMENSIONS],	// location[ coordinateIdx ]. On exit, the coordinateIdx'th coordinate of the location of the xi point.
	double tangent[MAX_NUM_DIMENSIONS],	// tangent[ coordinateIdx ]. On exit, the coordinateIdx'th coordinate of the tangent at the xi point.
	double normal[MAX_NUM_DIMENSIONS],	// normal[ coordinateIdx ]. On exit, the coordinateIdx'th coordiante of the normal at the xi point.
	double* Jacobian						// On exit, the Jacobian of the coordinate to xi transformation at the xi point.
	)
{
	int coordinateIdx, err = 0;

	*Jacobian = 0.0;
	// Interpolate the basis functions in this element to find the location, tangent and Jacobian at the xi point.
	for (coordinateIdx = 0; coordinateIdx < numDimensions; coordinateIdx++) {
		err = BasisFunctionInterpolateXi(elementType, BASIS_FUNCTIONS_NO_PARTIAL_DERIVATIVE, xi, elementDofs[coordinateIdx], &location[coordinateIdx]);
		if (err != BASIS_FUNCTIONS_NO_ERROR) return(err);
		err = BasisFunctionInterpolateXi(elementType, BASIS_FUNCTIONS_FIRST_PARTIAL_DERIVATIVE, xi, elementDofs[coordinateIdx], &(tangent[coordinateIdx]));
		if (err != BASIS_FUNCTIONS_NO_ERROR) return(err);
		*Jacobian += tangent[coordinateIdx] * tangent[coordinateIdx];
	}
	*Jacobian = sqrt(*Jacobian);
	// Calculate the normal vector by rotating the tangent vector.
	if (abs(*Jacobian) > ZERO_TOLERANCE) {
		normal[0] = tangent[1] / (*Jacobian);
		normal[1] = -tangent[0] / (*Jacobian);
	}
	else {
		return BEM_ZERO_JACOBIAN_ERROR;
	}

	return BEM_NO_ERROR;
}

// This routine calculates the A & B matrix entries for a given singular point
int CalculateMatrixEntries(
	const int numDimensions,			// The number of dimensions in the problem.
	const int elementType,				// The boundary element type in the mesh.
	const int numElements,				// The number of elements in the mesh.
	const double averageElementSize,	// The average size of elements in the mesh.
	const int analysisType,				// The type of analysis (equation type).
	const int numVariables,				// The number of variables per node.
	const int numDOFS,					// The number of degrees-of-freedom (DOFS) in the problem.
	int** elementNodes,					// elementNodes[ elementIdx ][ localNodeIdx ]. The node number for the localNodeIdx'th local node in the elementIdx'th element.
	double pLocation[MAX_NUM_DIMENSIONS],	// pLocation[ coordinateIdx ]. The coordinateIdx'th coordinate for the location of the singular point.
	double **geometry,					// geometry[ nodeIdx ][ coordinateIdx ]. The coordinateIdx'th geometric coordinate for the nodeIdx'th node.
	double** elementDOFS,				// elementDOFS[ coordinateIdx ][ localNodeIdx ]. Temporary working array to store the element DOFS for the coordinateIdx'th coordinate of the localNodeIdx'th local node in an element.
	int* numGaussPoints,				// numGaussPoints[ integrationSchemeIdx ]. The number of Gauss points in the integrationSchemeIdx'th integration scheme.
	double** gaussLocations,			// gaussLocations[ integrationSchemeIdx ][ gaussPointIdx ]. The xi location of the gaussPointIdx'th Gauss point in the integrationSchemeIdx'th integration scheme.
	double** gaussWeights,				// gaussWeights[ integrationSchemeIdx ][ gaussPointIdx ]. The Gauss weight of the gaussPointIdx'th Gauss point in the integrationSchemeIdx'th integration scheme.
	const int rowSumsFlag,				// Flag to indicate if row sums are used to calculate the diagonal entry in the A matrix.
	const int rowDofIdx,				// The row (DOF) index in the A & B matrices to calculate the matrix entries for.
	double** A,							// The BEM A matrix to calculate the matrix entries for.
	double** B							// The BEM B matrix to calculate the matrix entries for.
	)
{

	double Rmin,
		bLog = 0.0,
		bGauss = 0.0,
		bLogFlip = 0.0,
		bGaussFlip = 0.0,
		subElemA = 0.0,
		subElemB = 0.0,
		*R,
		s,
		xi,
		wgt,
		Jacobian,
		r = 0.0,						// The distance to evaluate the Green's function at
		drdn = 0.0,						// The value of dr/dn to evaluate the Green's function with
		aGauss,							
		rk = 0.0,
		rknk = 0.0,
		*location = NULL,
		*tangent = NULL,
		*normal = NULL; //elemet DOF's gives the actual coordiantes nodes in the elements
	double phi = 0.0;
	int integration,
		localNodeIdx,
		greensFunctionType = BEM_LAPLACE_GREENS_FUNCTION,
		gaussPointIdx;

	location = new double[numDimensions];
	tangent = new double[numDimensions];
	normal = new double[numDimensions];
	R = new double[elementType + 1];
	//******************************************************
	//*** ADD CODE HERE TO COMPUTE ROW OF A & B MATRICES ***
	//******************************************************
	for (int elem = 0; elem < numElements; elem++){


		//calculate Rmin whether it is closer to the local node 1 or two of the element
		for (int indx = 0; indx < elementType + 1; indx++){
			R[indx] = sqrt(pow(pLocation[0] - geometry[elementNodes[elem][indx]][0], 2) + pow(pLocation[1] - geometry[elementNodes[elem][indx]][1], 2));
		}
		Rmin = R[0];
		for (int indx = 1; indx < elementType + 1; indx++)
		{
			if (R[indx] < Rmin){
				Rmin = R[indx];
			}
		}

		s = Rmin / averageElementSize;


		// set the integration scheme based on the value of s which represents the strength of the singularity
		if (s <= 0.01){
			integration = BEM_INSIDE_ELEMENT_INTEGRATION_SCHEME; // Calculates the    
		}
		else if (s <= 0.1){
			integration = BEM_HIGH_GAUSS_INTEGRATION_SCHEME;
		}
		else if (s <= 5){
			integration = BEM_MEDIUM_GAUSS_INTEGRATION_SCHEME;
		}
		else if (s > 5){
			integration = BEM_LOW_GAUSS_INTEGRATION_SCHEME;
		}

		for (int coordinateIdx = 0; coordinateIdx < numDimensions; coordinateIdx++){   // Records the location of the local nodes... 
			//Get the element geometric information
			for (int localNodeIdx = 0; localNodeIdx < elementType + 1; localNodeIdx++) {
				elementDOFS[coordinateIdx][localNodeIdx] = geometry[elementNodes[elem][localNodeIdx]][coordinateIdx];
			}
		}

		/****************** For the case where the singularity is in the element being looked at **********************/

			if (integration == BEM_INSIDE_ELEMENT_INTEGRATION_SCHEME){  // if the singularity is in the element being evaluated

				if (R[0] < DBL_EPSILON){     // If the singularity is on the node 0

					//step through the local nodes of this element
					for (localNodeIdx = 0; localNodeIdx < elementType + 1; localNodeIdx++){

						aGauss = 0.0;
						
						//Use high Guass for A it will be overwritten by row sum when localNodeIdx is 0   
						for (int gaussPointIdx = 0; gaussPointIdx < numGaussPoints[BEM_HIGH_GAUSS_INTEGRATION_SCHEME]; gaussPointIdx++) {
							xi = gaussLocations[BEM_HIGH_GAUSS_INTEGRATION_SCHEME][gaussPointIdx];
							wgt = gaussWeights[BEM_HIGH_GAUSS_INTEGRATION_SCHEME][gaussPointIdx];

							BasisFunctionEvaluateXi(elementType, localNodeIdx, 0, xi, &phi);
							CalculateGeometricInformation(numDimensions, elementType, xi, elementDOFS, location, tangent, normal, &Jacobian);

							// dot product of rk and nk
							double rknk = 0.0;
							double r = 0.0;
							double rk = 0.0;
							for (int k = 0; k < numDimensions; k++){
								rk = (location[k] - pLocation[k]);
								rknk += rk * normal[k];
								r += (pow((pLocation[k] - location[k]), 2));
							}

							// divide by r
							r = sqrt(r);
							rknk = rknk / (r*r);
							rknk = rknk*phi* Jacobian*wgt;
							aGauss += rknk;
						}

						A[rowDofIdx][elementNodes[elem][localNodeIdx]] += aGauss*(-1.0 / (2.0 * PI));

						bGauss = 0.0;
						// use high gauss for log(r/xi) and add the log gauss
						for (gaussPointIdx = 0; gaussPointIdx < numGaussPoints[BEM_HIGH_GAUSS_INTEGRATION_SCHEME]; gaussPointIdx++) {
							xi = gaussLocations[BEM_HIGH_GAUSS_INTEGRATION_SCHEME][gaussPointIdx];
							wgt = gaussWeights[BEM_HIGH_GAUSS_INTEGRATION_SCHEME][gaussPointIdx];


							BasisFunctionEvaluateXi(elementType, localNodeIdx, 0, xi, &phi);
							CalculateGeometricInformation(numDimensions, elementType, xi, elementDOFS, location, tangent, normal, &Jacobian);
							double r = 0.0;
							for (int k = 0; k < numDimensions; k++){
								r += (pow((location[k] - pLocation[k]), 2));
							}
							r = sqrt(r);

							bGauss += phi* wgt * log(r / xi) * Jacobian;
						}


						bLog = 0.0;
						for (int gaussPointIdx = 0; gaussPointIdx < numGaussPoints[BEM_LOG_GAUSS_INTEGRATION_SCHEME]; gaussPointIdx++)
						{
							xi = gaussLocations[BEM_LOG_GAUSS_INTEGRATION_SCHEME][gaussPointIdx];
							wgt = gaussWeights[BEM_LOG_GAUSS_INTEGRATION_SCHEME][gaussPointIdx];

							BasisFunctionEvaluateXi(elementType, localNodeIdx, 0, xi, &phi);
							CalculateGeometricInformation(numDimensions, elementType, xi, elementDOFS, location, tangent, normal, &Jacobian);

							bLog += phi* Jacobian *wgt;
						}

						bGauss *= (-1.0 / (2.0 * PI));
						bLog *= (-1.0 / (2.0 * PI));
						B[rowDofIdx][elementNodes[elem][localNodeIdx]] += bGauss + bLog;
					}
				}

				else if(R[2]< DBL_EPSILON) {
					/*********** Singularity at end node of element*************/

					for (localNodeIdx = 0; localNodeIdx < elementType + 1; localNodeIdx++){

					aGauss = 0.0;
					//-----------------------------------------
					//Calculate A with high Gauss
					for (int gaussPointIdx = 0; gaussPointIdx < numGaussPoints[BEM_HIGH_GAUSS_INTEGRATION_SCHEME]; gaussPointIdx++) {
						xi = gaussLocations[BEM_HIGH_GAUSS_INTEGRATION_SCHEME][gaussPointIdx];
						wgt = gaussWeights[BEM_HIGH_GAUSS_INTEGRATION_SCHEME][gaussPointIdx];

						BasisFunctionEvaluateXi(elementType, localNodeIdx, 0, xi, &phi);
						CalculateGeometricInformation(numDimensions, elementType, xi, elementDOFS, location, tangent, normal, &Jacobian);

						// dot product of rk and nk
						double rknk = 0.0;
						double r = 0.0;
						double rk = 0.0;
						for (int k = 0; k < numDimensions; k++){
							rk = (location[k] - pLocation[k]);
							rknk += rk * normal[k];
							r += (pow((pLocation[k] - location[k]), 2));
						}

						// divide by r
						r = sqrt(r);
						rknk = rknk / (r*r);
						rknk = rknk*phi* Jacobian*wgt;
						aGauss += rknk;
					}

					A[rowDofIdx][elementNodes[elem][localNodeIdx]] += aGauss*(-1.0 / (2.0 * PI));


					//Flip axis for b with split Log
					bGauss = 0.0;
					for (int gaussPointIdx = 0; gaussPointIdx < numGaussPoints[BEM_HIGH_GAUSS_INTEGRATION_SCHEME]; gaussPointIdx++) {
						xi = gaussLocations[BEM_HIGH_GAUSS_INTEGRATION_SCHEME][gaussPointIdx];
						wgt = gaussWeights[BEM_HIGH_GAUSS_INTEGRATION_SCHEME][gaussPointIdx];

						BasisFunctionEvaluateXi(elementType, localNodeIdx, 0, xi, &phi);
						CalculateGeometricInformation(numDimensions, elementType, xi, elementDOFS, location, tangent, normal, &Jacobian);
						double r = 0.0;
						for (int k = 0; k < numDimensions; k++){
							r += (pow((pLocation[k] - location[k]), 2));
						}
						r = sqrt(r);

						bGauss += (phi * wgt * log(r / (1.0 - xi)) * Jacobian);
					}


					bLog = 0.0;
					for (int gaussPointIdx = 0; gaussPointIdx < numGaussPoints[BEM_LOG_GAUSS_INTEGRATION_SCHEME]; gaussPointIdx++) {
						xi = gaussLocations[BEM_LOG_GAUSS_INTEGRATION_SCHEME][gaussPointIdx];
						wgt = gaussWeights[BEM_LOG_GAUSS_INTEGRATION_SCHEME][gaussPointIdx];

						xi = 1.0 - xi;
						BasisFunctionEvaluateXi(elementType, localNodeIdx, 0, xi, &phi);
						CalculateGeometricInformation(numDimensions, elementType, xi, elementDOFS, location, tangent, normal, &Jacobian);

						bLog += phi * Jacobian *wgt;
					}
					bGauss = bGauss * (-1.0 / (2.0 * PI));
					bLog = bLog * (-1.0 / (2.0 * PI));
					B[rowDofIdx][elementNodes[elem][localNodeIdx]] += bGauss + bLog;

				}
			}
				
				/***************If singularity is on middle node of element**************/
				
				else if (R[1] < DBL_EPSILON){ 
					
					for (localNodeIdx = 0; localNodeIdx < elementType + 1; localNodeIdx++){
					
						aGauss = 0.0;
						//-----------------------------------------
						//Calculate A with high Gauss
						for (int gaussPointIdx = 0; gaussPointIdx < numGaussPoints[BEM_HIGH_GAUSS_INTEGRATION_SCHEME]; gaussPointIdx++) {
							xi = gaussLocations[BEM_HIGH_GAUSS_INTEGRATION_SCHEME][gaussPointIdx];
							wgt = gaussWeights[BEM_HIGH_GAUSS_INTEGRATION_SCHEME][gaussPointIdx];

							BasisFunctionEvaluateXi(elementType, localNodeIdx, 0, xi, &phi);
							CalculateGeometricInformation(numDimensions, elementType, xi, elementDOFS, location, tangent, normal, &Jacobian);

							// dot product of rk and nk
							double rknk = 0.0;
							double r = 0.0;
							double rk = 0.0;
							for (int k = 0; k < numDimensions; k++){
								rk = (location[k] - pLocation[k]);
								rknk += rk * normal[k];
								r += (pow((pLocation[k] - location[k]), 2));
							}

							// divide by r
							r = sqrt(r);
							rknk = rknk / (r*r);
							rknk = rknk*phi* Jacobian*wgt;
							aGauss += rknk;
						}

						A[rowDofIdx][elementNodes[elem][localNodeIdx]] += aGauss*(-1.0 / (2.0 * PI));

						/************ split element for b matrix *************/
							// sub Element A
							// use flipped log

							bGaussFlip = 0.0;
							for (int gaussPointIdx = 0; gaussPointIdx < numGaussPoints[BEM_HIGH_GAUSS_INTEGRATION_SCHEME]; gaussPointIdx++) {
								xi = gaussLocations[BEM_HIGH_GAUSS_INTEGRATION_SCHEME][gaussPointIdx];
								wgt = gaussWeights[BEM_HIGH_GAUSS_INTEGRATION_SCHEME][gaussPointIdx];

								BasisFunctionEvaluateXi(elementType, localNodeIdx, 0, xi / 2.0, &phi);
								CalculateGeometricInformation(numDimensions, elementType, xi / 2.0, elementDOFS, location, tangent, normal, &Jacobian);

								double r = 0.0;
								for (int k = 0; k < numDimensions; k++){
									r += (pow((pLocation[k] - location[k]), 2));
								}
								r = sqrt(r);

								bGaussFlip += (phi * wgt * log(r / (1.0 - xi)) * Jacobian);
							}


							bLogFlip = 0.0;
							for (int gaussPointIdx = 0; gaussPointIdx < numGaussPoints[BEM_LOG_GAUSS_INTEGRATION_SCHEME]; gaussPointIdx++) {
								xi = gaussLocations[BEM_LOG_GAUSS_INTEGRATION_SCHEME][gaussPointIdx];
								wgt = gaussWeights[BEM_LOG_GAUSS_INTEGRATION_SCHEME][gaussPointIdx];

								BasisFunctionEvaluateXi(elementType, localNodeIdx, 0, (1.0 - xi) / 2.0, &phi);
								CalculateGeometricInformation(numDimensions, elementType, (1.0 - xi) / 2.0, elementDOFS, location, tangent, normal, &Jacobian);

								bLogFlip += (phi * wgt * Jacobian);
							}
							bGaussFlip = bGaussFlip * (-1.0 / (2.0 * PI));
							bLogFlip = bLogFlip * (-1.0 / (2.0 * PI));
							subElemA = bGaussFlip + bLogFlip;


							// sub Element B
							//use xi and log

							bGauss = 0.0;
							for (int gaussPointIdx = 0; gaussPointIdx < numGaussPoints[BEM_HIGH_GAUSS_INTEGRATION_SCHEME]; gaussPointIdx++) {
								xi = gaussLocations[BEM_HIGH_GAUSS_INTEGRATION_SCHEME][gaussPointIdx];
								wgt = gaussWeights[BEM_HIGH_GAUSS_INTEGRATION_SCHEME][gaussPointIdx];

								BasisFunctionEvaluateXi(elementType, localNodeIdx, 0, (xi + 1.0) / 2.0, &phi);
								CalculateGeometricInformation(numDimensions, elementType, (xi + 1.0) / 2.0, elementDOFS, location, tangent, normal, &Jacobian);

								double r = 0.0;
								for (int k = 0; k < numDimensions; k++){
									r += (pow((pLocation[k] - location[k]), 2));
								}
								r = sqrt(r);

								bGauss += (phi * wgt * log(r / xi) * Jacobian);
							}


							bLog = 0.0;
							for (int gaussPointIdx = 0; gaussPointIdx < numGaussPoints[BEM_LOG_GAUSS_INTEGRATION_SCHEME]; gaussPointIdx++) {
								xi = gaussLocations[BEM_LOG_GAUSS_INTEGRATION_SCHEME][gaussPointIdx];
								wgt = gaussWeights[BEM_LOG_GAUSS_INTEGRATION_SCHEME][gaussPointIdx];

								BasisFunctionEvaluateXi(elementType, localNodeIdx, 0, (xi + 1.0) / 2.0, &phi);
								CalculateGeometricInformation(numDimensions, elementType, (xi + 1.0) / 2.0, elementDOFS, location, tangent, normal, &Jacobian);


								bLog += phi * Jacobian * wgt;
							}
							bGauss = bGauss * (-1.0 / (2.0 * PI));
							bLog = bLog * (-1.0 / (2.0 * PI));
							subElemB = bGauss + bLog;

							B[rowDofIdx][elementNodes[elem][localNodeIdx]] += (subElemA + subElemB) * 0.5;
					}
				}
			}
			/*********************Finish Singularity Element**************************/
			else {

				for (localNodeIdx = 0; localNodeIdx < elementType + 1; localNodeIdx++){

				// do norml gauss with appropriate number of points for both A and B
				double aGauss = 0.0;
				bGauss = 0.0;
				//-----------------------------------------
				//Summing gauss points
				for (int gaussPointIdx = 0; gaussPointIdx < numGaussPoints[integration]; gaussPointIdx++) {
					xi = gaussLocations[integration][gaussPointIdx];
					wgt = gaussWeights[integration][gaussPointIdx];

					BasisFunctionEvaluateXi(elementType, localNodeIdx, 0, xi, &phi);
					CalculateGeometricInformation(numDimensions, elementType, xi, elementDOFS, location, tangent, normal, &Jacobian);

					// dot product of rk and nk
					double rknk = 0.0;
					double r = 0.0;
					double rk = 0.0;
					for (int k = 0; k < numDimensions; k++){
						rk = (location[k] - pLocation[k]);
						rknk += rk * normal[k];
						r += (pow((pLocation[k] - location[k]), 2));
					}

					// divide by r
					r = sqrt(r);
					rknk = rknk / (r*r);
					rknk = rknk*phi* Jacobian*wgt;
					aGauss += rknk;
					bGauss += phi * log(r)*Jacobian*wgt;
				}
				A[rowDofIdx][elementNodes[elem][localNodeIdx]] += aGauss*(-1.0 / (2.0 * PI));
				B[rowDofIdx][elementNodes[elem][localNodeIdx]] += bGauss* (-1.0 / (2.0 * PI));

			}
			}
	}

	if (rowSumsFlag == 1){
		A[rowDofIdx][rowDofIdx] = 0.0;

		for (int l = 0; l < numDOFS; l++){

			if (l != rowDofIdx)
				A[rowDofIdx][rowDofIdx] += -A[rowDofIdx][l];
		}
	}

	delete location, tangent, normal;


	return BEM_NO_ERROR;


}

// Boundry Conditions
int BoundaryConditions(double **A, double **B, int **nodeBoundaryConditions, double *f, double *u, double *q, int numNodes, double **B_mat, double **C){
	double *BC_vect = NULL;
	int err = BEM_NO_ERROR;

	BC_vect = new double[numNodes];

	for (int i = 0; i < numNodes; i++){

		if (nodeBoundaryConditions[i][0] == 0){
			// error need either a Dirichlet or Neumann BC
			err = BEM_IS_ERROR;
		}

		else if (nodeBoundaryConditions[i][0] == 1){
			BC_vect[i] = u[i]; // dirichlet BC
			for (int j = 0; j < numNodes; j++){
				B_mat[j][i] = -A[j][i];
				C[j][i] = -B[j][i];
			}
		}

		else if (nodeBoundaryConditions[i][0] == 2){
			BC_vect[i] = q[i]; // Neumann BC
			for (int j = 0; j < numNodes; j++){
				B_mat[j][i] = B[j][i];
				C[j][i] = A[j][i];
			}
		}
	}

	for (int i = 0; i < numNodes; i++){
		for (int j = 0; j < numNodes; j++){
			f[i] += B_mat[i][j] * BC_vect[j];
		}
	}
	return err;
}


int Rearranging(int **nodeBoundaryConditions, double *u, double *q, double *x, int numNodes){
	int err = BEM_NO_ERROR;
	for (int i = 0; i < numNodes; i++){

		if (nodeBoundaryConditions[i][0] == 0){

			err = BEM_IS_ERROR;

		}

		else if (nodeBoundaryConditions[i][0] == 1){

			q[i] = x[i];


		}
		else if (nodeBoundaryConditions[i][0] == 2){

			u[i] = x[i];

		}
	}

	return err;
}


// Setup integration schemes
int IntegrationSchemesSetup(
	int **numGaussPoints,			// numGaussPoints[ integrationSchemeIdx ]. On exit, the number of Gauss points for the integrationIdx'th integration scheme.
	double ***gaussLocations,		// gaussLocations[ integrationSchemeIdx ][ gaussPointIdx ]. On exit, the xi location for the gaussPointIdx'th gauss point in the integrationSchemeIdx'th integration scheme.
	double ***gaussWeights			// gaussWeights[ integrationSchemeIdx ][ gaussPointIdx ]. On exit, the weight for the gaussPointIdx'th gauss point in the integrationSchemeIdx'th integration scheme.
	)
{
	// Local variables
	int err;

	// Check incoming pointers
	if (*numGaussPoints != NULL) {
		cout << "ERROR: numGaussPoints pointer is not NULL." << endl;
		BEM_ERROR
	}
	if (*gaussLocations != NULL) {
		cout << "ERROR: gaussLocations pointer is not NULL." << endl;
		BEM_ERROR
	}
	if (*gaussWeights != NULL) {
		cout << "ERROR: gaussWeights pointer is not NULL." << endl;
		BEM_ERROR
	}

	// Allocate arrays
	*numGaussPoints = new int[BEM_NUMBER_GAUSS_SCHEMES];
	*gaussLocations = new double*[BEM_NUMBER_GAUSS_SCHEMES];
	*gaussWeights = new double*[BEM_NUMBER_GAUSS_SCHEMES];
	// Setup logarithmic Gauss scheme
	(*numGaussPoints)[BEM_LOG_GAUSS_INTEGRATION_SCHEME] = BEM_NUMBER_LOG_GAUSS_POINTS;
	(*gaussLocations)[BEM_LOG_GAUSS_INTEGRATION_SCHEME] = NULL;
	(*gaussWeights)[BEM_LOG_GAUSS_INTEGRATION_SCHEME] = NULL;
	err = GaussianQuadratureLogarithmicPointsCreate(BEM_NUMBER_LOG_GAUSS_POINTS, &((*gaussLocations)[BEM_LOG_GAUSS_INTEGRATION_SCHEME]), &((*gaussWeights)[BEM_LOG_GAUSS_INTEGRATION_SCHEME]));
	if (err != GAUSSIAN_QUADRATURE_NO_ERROR) {
		cout << "ERROR: Error code " << err << " when computing logarithmic Gaussian quadrature scheme." << endl;
		BEM_ERROR
	}
	// Setup low Gauss scheme
	(*numGaussPoints)[BEM_LOW_GAUSS_INTEGRATION_SCHEME] = BEM_NUMBER_LOW_GAUSS_POINTS;
	(*gaussLocations)[BEM_LOW_GAUSS_INTEGRATION_SCHEME] = NULL;
	(*gaussWeights)[BEM_LOW_GAUSS_INTEGRATION_SCHEME] = NULL;
	err = GaussianQuadratureLegendrePointsCreate(BEM_NUMBER_LOW_GAUSS_POINTS, 0.0, 1.0, &((*gaussLocations)[BEM_LOW_GAUSS_INTEGRATION_SCHEME]), &((*gaussWeights)[BEM_LOW_GAUSS_INTEGRATION_SCHEME]));
	if (err != GAUSSIAN_QUADRATURE_NO_ERROR) {
		cout << "ERROR: Error code " << err << " when computing low order Gaussian quadrature scheme." << endl;
		BEM_ERROR
	}
	// Setup medium Gauss scheme
	(*numGaussPoints)[BEM_MEDIUM_GAUSS_INTEGRATION_SCHEME] = BEM_NUMBER_MEDIUM_GAUSS_POINTS;
	(*gaussLocations)[BEM_MEDIUM_GAUSS_INTEGRATION_SCHEME] = NULL;
	(*gaussWeights)[BEM_MEDIUM_GAUSS_INTEGRATION_SCHEME] = NULL;
	err = GaussianQuadratureLegendrePointsCreate(BEM_NUMBER_MEDIUM_GAUSS_POINTS, 0.0, 1.0, &((*gaussLocations)[BEM_MEDIUM_GAUSS_INTEGRATION_SCHEME]), &((*gaussWeights)[BEM_MEDIUM_GAUSS_INTEGRATION_SCHEME]));
	if (err != GAUSSIAN_QUADRATURE_NO_ERROR) {
		cout << "ERROR: Error code " << err << " when computing medium order Gaussian quadrature scheme." << endl;
		BEM_ERROR
	}
	// Setup high Gauss scheme
	(*numGaussPoints)[BEM_HIGH_GAUSS_INTEGRATION_SCHEME] = BEM_NUMBER_HIGH_GAUSS_POINTS;
	(*gaussLocations)[BEM_HIGH_GAUSS_INTEGRATION_SCHEME] = NULL;
	(*gaussWeights)[BEM_HIGH_GAUSS_INTEGRATION_SCHEME] = NULL;
	err = GaussianQuadratureLegendrePointsCreate(BEM_NUMBER_HIGH_GAUSS_POINTS, 0.0, 1.0, &((*gaussLocations)[BEM_HIGH_GAUSS_INTEGRATION_SCHEME]), &((*gaussWeights)[BEM_HIGH_GAUSS_INTEGRATION_SCHEME]));
	if (err != GAUSSIAN_QUADRATURE_NO_ERROR) {
		cout << "ERROR: Error code " << err << " when computing high order Gaussian quadrature scheme." << endl;
		BEM_ERROR
	}

	return BEM_NO_ERROR;
}

// Finalise the integration schemes
int IntegrationSchemesFinalise(
	int **numGaussPoints,			// The number of Gauss points array to finalise
	double ***gaussLocations,		// The Gauss point locations array to finalise
	double ***gaussWeights			// The Gauss weights array to finalise
	)
{
	// Local variables
	int integrationSchemeIdx;

	if (*numGaussPoints != NULL) delete[] * numGaussPoints;
	if (*gaussLocations != NULL) {
		for (integrationSchemeIdx = 0; integrationSchemeIdx < BEM_NUMBER_GAUSS_SCHEMES; integrationSchemeIdx++) {
			if ((*gaussLocations)[integrationSchemeIdx] != NULL) delete[](*gaussLocations)[integrationSchemeIdx];
		}
		delete[] * gaussLocations;
	}
	if (*gaussWeights != NULL) {
		for (integrationSchemeIdx = 0; integrationSchemeIdx < BEM_NUMBER_GAUSS_SCHEMES; integrationSchemeIdx++) {
			if ((*gaussWeights)[integrationSchemeIdx] != NULL) delete[](*gaussWeights)[integrationSchemeIdx];
		}
		delete[] * gaussWeights;
	}

	return BEM_NO_ERROR;
}

// Calculates the value of the freespace Green's function
int GreensFunction(
	const int greensFunctionType,	// The type of Green's function to evaluate
	const double r,					// The distance to evaluate the Green's function at
	double* greensFunction			// On return, the value of the evaluated Green's function
	)
{
	switch (greensFunctionType) {
	case BEM_LAPLACE_GREENS_FUNCTION:
		if (r > ZERO_TOLERANCE) {
			*greensFunction = -1.0 / (2.0*PI)*log(r);
		}
		else {
			return BEM_ZERO_DISTANCE_ERROR;
		}
		break;
	case BEM_LOG_LAPLACE_GREENS_FUNCTION:
		*greensFunction = -1.0 / (2.0*PI);
		break;
	default:
		return BEM_INVALID_GREENS_FUNCTION_IDX_ERROR;
		break;
	}

	return BEM_NO_ERROR;
}

// Calculates the value of the derivative of the freespace Green's function
int DerivativeGreensFunction(
	const int greensFunctionType,		// The type of Green's function to evaluate
	const double r,						// The distance to evaluate the Green's function at
	const double drdn,					// The value of dr/dn to evaluate the Green's function with
	double* derivativeGreensFunction	// On return, the value of the evaluated derivative of the Green's function
	)
{
	switch (greensFunctionType) {
	case BEM_LAPLACE_GREENS_FUNCTION:
		if (r > ZERO_TOLERANCE) {
			*derivativeGreensFunction = -1.0 / (2.0*PI*r)*drdn;
		}
		else {
			return BEM_ZERO_DISTANCE_ERROR;
		}
		break;
	default:
		return BEM_INVALID_GREENS_FUNCTION_IDX_ERROR;
		break;
	}

	return BEM_NO_ERROR;
}
