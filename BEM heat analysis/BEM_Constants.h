#ifndef BEM_CONSTANTS

using namespace std;

// Valid error codes.
#define BEM_NO_ERROR 0							// No error.
#define BEM_IS_ERROR 1							// An error occured.

// Setup error handling macro.
//#define BEM_ERROR return BEM_IS_ERROR;		// Just return the error code.
#define BEM_ERROR PAUSE; return BEM_IS_ERROR;	// Pause and return error code.

#define MAX_NUM_DIMENSIONS 3					// The maximum number of dimensions

// Valid analysis types
#define LAPLACE_ANALYSIS_TYPE 1					// A Laplace equation problem/analysis.
#define LINEAR_ELASTICITY_ANALYSIS_TYPE 2		// A linear elasticity problem/analysis.

// Valid mesh types.
#define READIN_MESH 0							// The mesh has been read in from a file.
#define CIRCLE_GENERATED_MESH 1					// The mesh is a circle generated mesh.
#define PLATE_GENERATED_MESH 2					// The mesh is a plate with a hole generated mesh.

// Valid analytic test types.
#define NO_ANALYTIC_TEST 0						// No analytic test.
#define U_ANALYTIC_TEST 1						// Dirichlet analytic test. Set a Dirichlet BC at all nodes and solve for q.
#define Q_ANALYTIC_TEST 2						// von Neumann analytic test. Set a Dirichlet BC at the first node and von Neumann BCs at all other nodes and solve for u.

// Valid node boundary condition types.
#define NODE_NO_BC 0							// The node does not have any BCs set.
#define NODE_ESSENTIAL_BC 1						// The node has an essential (Dirichlet) BC set.
#define NODE_NATURAL_BC 2						// The node has a natural (von Neumann) BC set.

// Valid corner types.
#define NODE_NO_CORNER 0						// The node is not at a corner.
#define NODE_90DEG_CORNER 1						// The node corresponds to a 90 degree corner.
#define NODE_270DEG_CORNER 2					// The node corresponds to a 270 degree corner.

#define BEM_CONSTANTS
#endif
